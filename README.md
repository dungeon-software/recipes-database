# PV168 Project: Recipes DB

A desktop application made for tracking recipes. 
The application will allow browsing through recipes, track ingredients and instructions.
Additionally, it will contain information about their nutritional value.

## Team Information

| Seminar Group | Team |
|---------------|------|
| PV168/04      | 02   |

### Members

| Role           | Person                                                  |
|----------------|---------------------------------------------------------|
|Team Lead       | [Bc. Vít Šebela](https://is.muni.cz/auth/osoba/531030)  |
|Member          | [Stanislav Zeman](https://is.muni.cz/auth/osoba/524881) |
|Member          | [Martin Samec](https://is.muni.cz/auth/osoba/505829)    |
|Member          | [Lukáš Málik](https://is.muni.cz/auth/osoba/514189)     |

### Evaluators

| Role           | Person                                                           |
|----------------|------------------------------------------------------------------|
|Customer        | [Mgr. Vendula Teuchnerová](https://is.muni.cz/auth/osoba/445583) |
|Technical Coach | [Mgr. Vojtěch Sassmann](https://is.muni.cz/auth/osoba/445320)    |