package cz.muni.fi.pv168.recipesdb.data.storage.database;

import cz.muni.fi.pv168.recipesdb.data.DataAccess;
import cz.muni.fi.pv168.recipesdb.data.storage.dao.CategoryDao;
import cz.muni.fi.pv168.recipesdb.data.storage.dao.IngredientDao;
import cz.muni.fi.pv168.recipesdb.data.storage.dao.RecipeDao;
import cz.muni.fi.pv168.recipesdb.data.storage.dao.UnitDao;
import cz.muni.fi.pv168.recipesdb.data.storage.mapper.CategoryMapper;
import cz.muni.fi.pv168.recipesdb.data.storage.mapper.IngredientMapper;
import cz.muni.fi.pv168.recipesdb.data.storage.mapper.RecipeMapper;
import cz.muni.fi.pv168.recipesdb.data.storage.mapper.UnitMapper;
import cz.muni.fi.pv168.recipesdb.data.storage.repository.CategoryRepository;
import cz.muni.fi.pv168.recipesdb.data.storage.repository.IngredientRepository;
import cz.muni.fi.pv168.recipesdb.data.storage.repository.RecipeRepository;
import cz.muni.fi.pv168.recipesdb.data.storage.repository.UnitRepository;
import cz.muni.fi.pv168.recipesdb.data.validation.CategoryValidator;
import cz.muni.fi.pv168.recipesdb.data.validation.IngredientValidator;
import cz.muni.fi.pv168.recipesdb.data.validation.RecipeValidator;
import cz.muni.fi.pv168.recipesdb.data.validation.UnitValidator;
import cz.muni.fi.pv168.recipesdb.model.Category;
import cz.muni.fi.pv168.recipesdb.model.CategoryColors;
import cz.muni.fi.pv168.recipesdb.model.Ingredient;
import cz.muni.fi.pv168.recipesdb.model.Recipe;
import cz.muni.fi.pv168.recipesdb.model.Unit;
import cz.muni.fi.pv168.recipesdb.model.UnitConversion;
import cz.muni.fi.pv168.recipesdb.model.WeightedIngredient;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * @author Vít Šebela
 */
public class DatabaseTest {

    private DatabaseManager databaseManager;

    @BeforeEach
    void setUp() {
        this.databaseManager = DatabaseManager.createTestInstance();
        this.databaseManager.initData("dev");
    }

    @AfterEach
    void tearDown() {
        this.databaseManager.destroySchema();
    }

    @Test
    public void recipeDaoCreateTest() {
        var ingredientValidator = new IngredientValidator();
        var recipeValidator = new RecipeValidator();
        var categoryValidator = new CategoryValidator();
        var unitValidator = new UnitValidator();
        var ingredientDao = new IngredientDao(databaseManager::getConnectionHandler);
        var categoryDao = new CategoryDao(databaseManager::getConnectionHandler);
        var unitDao = new UnitDao(databaseManager::getConnectionHandler);
        var recipeDao = new RecipeDao(databaseManager::getConnectionHandler);

        var categoryMapper = new CategoryMapper(categoryValidator);
        var categoryRepository = new CategoryRepository(categoryDao, categoryMapper);

        var unitMapper = new UnitMapper(unitValidator);
        var unitRepository = new UnitRepository(unitDao, unitMapper);

        var ingredientMapper = new IngredientMapper(unitRepository, ingredientValidator);
        var ingredientRepository = new IngredientRepository(ingredientDao, ingredientMapper);

        var recipeMapper = new RecipeMapper(ingredientRepository, categoryRepository, recipeValidator);
        var recipeRepository = new RecipeRepository(recipeDao, recipeMapper);

        categoryRepository.create(new Category("kategorie1", CategoryColors.GREEN));

        var kg = DataAccess.getBaseUnit("KG");
        var g = DataAccess.getBaseUnit("G");

        unitRepository.create(
                new Unit(
                        "Unita1",
                        "un1",
                        new UnitConversion(
                                20.0,
                                kg)));

        unitRepository.create(
                new Unit(
                        "Unita2",
                        "un2",
                        new UnitConversion(
                                40.0,
                                g)));

        unitRepository.findAll().forEach(System.out::println);

        ingredientRepository.create(new Ingredient("Ingredience1", new Unit(
                "Unita1",
                "un1",
                new UnitConversion(
                        20.0,
                        kg)),
                240));

        ingredientRepository.create(new Ingredient("Ingredience2", new Unit(
                "Unita2",
                "un2",
                new UnitConversion(
                        40.0,
                        g)),
                120));

        recipeRepository.create(new Recipe(
                "test",
                new Category("kategorie1", CategoryColors.GREEN),
                10,
                4,
                "toto je krátký popis",
                "toto jsou krátké instrukce",
                List.of(
                        new WeightedIngredient(
                                new Ingredient("Ingredience1",
                                        new Unit("Unita1", "un1",
                                                new UnitConversion(
                                                        20.0,
                                                        kg)),
                                        240),
                                130.0),
                        new WeightedIngredient(
                                new Ingredient("Ingredience2",
                                        new Unit("Unita2", "un2",
                                                new UnitConversion(
                                                        40.0,
                                                        g)),
                                        240),
                                120.0))));
        recipeRepository.deleteIngredientByName("test", "Ingredience1");

        recipeRepository.findAll().forEach(System.out::println);

    }
}
