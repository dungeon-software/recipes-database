package cz.muni.fi.pv168.recipesdb.data.manipulation;

import java.util.logging.Level;
import java.util.logging.Logger;

public class DataManipulationException extends RuntimeException{
    DataManipulationException(String message, Throwable cause) {
        super(message, cause);
        Logger.getLogger("Data-manipulation").log(Level.SEVERE, message, cause);
    }
}
