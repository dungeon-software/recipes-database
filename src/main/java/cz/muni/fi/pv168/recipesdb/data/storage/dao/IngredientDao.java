package cz.muni.fi.pv168.recipesdb.data.storage.dao;

import cz.muni.fi.pv168.recipesdb.data.storage.DataStorageException;
import cz.muni.fi.pv168.recipesdb.data.storage.database.ConnectionHandler;
import cz.muni.fi.pv168.recipesdb.data.storage.valueobject.IngredientValueObject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * @author Vít Šebela
 */
public class IngredientDao implements Dao<IngredientValueObject> {
    private final Supplier<ConnectionHandler> connections;

    public IngredientDao(Supplier<ConnectionHandler> connections) {
        this.connections = connections;
    }

    @Override
    public IngredientValueObject create(IngredientValueObject entity) {
        String query = """
                    INSERT INTO Ingredient (name, unit, nutritionalValue)
                    VALUES (?,?,?);
                    """;
        try (var conn = connections.get();
             var preparedStatement = conn.use().prepareStatement(query)) {
            preparedStatement.setString(1, entity.name());
            preparedStatement.setString(2, entity.unit());
            preparedStatement.setDouble(3, entity.nutritionalValue());

            preparedStatement.executeUpdate();

            return findByName(entity.name()).orElseThrow();

        } catch (SQLException e) {
            throw new DataStorageException("Failed to create new ingredient", e);
        }
    }

    @Override
    public Collection<IngredientValueObject> findAll() {
        String query = """
                    SELECT name, unit, nutritionalValue
                    FROM Ingredient
                    """;
        try (var conn = connections.get();
             var preparedStatement =
                     conn.use().prepareStatement(query)) {

            List<IngredientValueObject> ingredients = new ArrayList<>();
            try (var resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    var ingredient = getIngredientsFromResultSet(resultSet);
                    ingredients.add(ingredient);
                }
            }

            return List.copyOf(ingredients);

        } catch (SQLException e) {
            throw new DataStorageException("Could not read from database", e);
        }
    }

    @Override
    public Optional<IngredientValueObject> findByName(String name) {
        String query = """
                    SELECT name, unit, nutritionalValue
                    FROM Ingredient
                    WHERE name = ?;
                    """;
        try (var conn = connections.get();
             var preparedStatement =
                     conn.use().prepareStatement(query)){

            preparedStatement.setString(1,name);

            try (var resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(getIngredientsFromResultSet(resultSet));
                }
                return Optional.empty();
            }

        } catch (SQLException e) {
            throw new DataStorageException("Could not read from database",e);
        }
    }

    @Override
    public IngredientValueObject update(IngredientValueObject entity) {
        String query = """
                    UPDATE Ingredient
                    SET (unit, nutritionalValue) = (?,?)
                    WHERE name = ?;
                    """;
        try (var conn = connections.get();
             var preparedStatement =
                     conn.use().prepareStatement(query)){

            preparedStatement.setString(1,entity.unit());
            preparedStatement.setDouble(2,entity.nutritionalValue());
            preparedStatement.setString(3, entity.name());
            preparedStatement.executeUpdate();

            return findByName(entity.name()).orElseThrow();

        } catch (SQLException e) {
            throw new DataStorageException("Failed to update ingredient in database",e);
        }
    }

    @Override
    public void deleteByName(String name) {
        String query = """
                    DELETE FROM Ingredient
                    WHERE name = ?
                    """;
        try (var conn = connections.get();
             var preparedStatement =
                     conn.use().prepareStatement(query)){

            preparedStatement.setString(1,name);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DataStorageException("Could not delete ingredient by name",e);
        }
    }

    private IngredientValueObject getIngredientsFromResultSet(ResultSet resultSet) throws SQLException {
        return new IngredientValueObject(
                resultSet.getString(1),
                resultSet.getString(2),
                resultSet.getInt(3)
        );
    }
}
