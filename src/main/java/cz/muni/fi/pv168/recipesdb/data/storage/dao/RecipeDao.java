package cz.muni.fi.pv168.recipesdb.data.storage.dao;

import cz.muni.fi.pv168.recipesdb.data.storage.DataStorageException;
import cz.muni.fi.pv168.recipesdb.data.storage.database.ConnectionHandler;
import cz.muni.fi.pv168.recipesdb.data.storage.valueobject.RecipeValueObject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * @author Vít Šebela
 */
public class RecipeDao implements Dao<RecipeValueObject> {
    private final Supplier<ConnectionHandler> connections;

    public RecipeDao(Supplier<ConnectionHandler> connections) {
        this.connections = connections;
    }

    @Override
    public RecipeValueObject create(RecipeValueObject entity) {
        String query = """
                    INSERT INTO Recipe (name, category, estimatedDuration, portions, description, instructions)
                    VALUES (?,?,?,?,?,?);
                    """;
        try (var conn = connections.get();
             var preparedStatement = conn.use().prepareStatement(query)) {

            preparedStatement.setString(1, entity.name());
            preparedStatement.setString(2, entity.category());
            preparedStatement.setInt(3, entity.estimatedDuration());
            preparedStatement.setInt(4, entity.portions());
            preparedStatement.setString(5, entity.description());
            preparedStatement.setString(6, entity.instruction());

            preparedStatement.executeUpdate();

            createIngredientsForRecipe(entity);

        } catch (SQLException e) {
            throw new DataStorageException("Failed to create new recipe", e);
        }

        return findByName(entity.name()).orElseThrow();
    }

    public void createIngredientsForRecipe(RecipeValueObject entity) {
        String query = """
                    INSERT INTO RecipeIngredients (recipe, ingredient, amount)
                    VALUES (?,?,?);
                    """;
        try (var conn = connections.get();
             var preparedStatement = conn.use().prepareStatement(query)) {

            for (var ingredient : entity.ingredients().entrySet()) {
                preparedStatement.setString(1, entity.name());
                preparedStatement.setString(2, ingredient.getKey());
                preparedStatement.setDouble(3, ingredient.getValue());
                preparedStatement.addBatch();
            }

            preparedStatement.executeBatch();

        } catch (SQLException e) {
            throw new DataStorageException("Failed to create ingredients of recipe", e);
        }
    }

    @Override
    public Collection<RecipeValueObject> findAll() {
        String query = """
                    SELECT name, category, estimatedDuration, portions, description, instructions
                    FROM Recipe
                    """;
        try (var conn = connections.get();
             var preparedStatement =
                     conn.use().prepareStatement(query)) {

            List<RecipeValueObject> recipeValueObjects = new ArrayList<>();
            try (var resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    recipeValueObjects.add(getRecipeWithIngredients(resultSet));
                }
            }

            return List.copyOf(recipeValueObjects);

        } catch (SQLException e) {
            throw new DataStorageException("Could not read from database", e);
        }
    }

    @Override
    public Optional<RecipeValueObject> findByName(String name) {
        String query = """
                    SELECT name, category, estimatedDuration, portions, description, instructions
                    FROM Recipe
                    WHERE name = ?;
                    """;
        try (var conn = connections.get();
             var preparedStatement =
                     conn.use().prepareStatement(query)) {

            preparedStatement.setString(1, name);

            try (var resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(getRecipeWithIngredients(resultSet));
                }
                return Optional.empty();
            }

        } catch (SQLException e) {
            throw new DataStorageException("Could not read from database", e);
        }
    }

    @Override
    public RecipeValueObject update(RecipeValueObject entity) {
        String query = """
                    UPDATE Recipe
                    SET (category, estimatedDuration, portions, description, instructions) = (?,?,?,?,?)
                    WHERE name = ?;
                    """;
        try (var conn = connections.get();
             var preparedStatement =
                     conn.use().prepareStatement(query)) {

            preparedStatement.setString(1, entity.category());
            preparedStatement.setInt(2, entity.estimatedDuration());
            preparedStatement.setInt(3, entity.portions());
            preparedStatement.setString(4, entity.description());
            preparedStatement.setString(5, entity.instruction());
            preparedStatement.setString(6, entity.name());

            preparedStatement.executeUpdate();

//            updateRecipeIngredientsSql(entity);

            return findByName(entity.name()).orElseThrow();

        } catch (SQLException e) {
            throw new DataStorageException("Failed to update recipe in database", e);
        }
    }

    @Override
    public void deleteByName(String name) {
        String queryByRecipe = """
                    DELETE FROM RecipeIngredients
                    WHERE recipe = ?
                    """;
        deleteFrom(name,Optional.empty(), queryByRecipe);
        String queryByName = """
                    DELETE FROM Recipe
                    WHERE name = ?
                    """;
        deleteFrom(name, Optional.empty(),queryByName);
    }

    public void deleteIngredientsByRecipe(String recipe) {
        String query = """
                    DELETE FROM RecipeIngredients
                    WHERE recipe = ?
                    """;
        deleteFrom(recipe,Optional.empty(), query);
    }

    public void deleteIngredientByName(String recipe, String ingredient) {
        String query = """
                    DELETE FROM RecipeIngredients
                    WHERE recipe = ? AND ingredient = ?
                    """;
        deleteFrom(recipe, Optional.of(ingredient), query);
    }

    private void deleteFrom(String recipe, Optional<String> ingredient, String sql) {
        try (var conn = connections.get();
             var preparedStatement =
                     conn.use().prepareStatement(sql)) {

            preparedStatement.setString(1, recipe);

            if (ingredient.isPresent()) {
                preparedStatement.setString(2, ingredient.get());
            }
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DataStorageException("Could not delete from database", e);
        }
    }

    private void updateRecipeIngredientsSql(RecipeValueObject recipeValueObject) {
        String query = """
                    UPDATE RecipeIngredients
                    SET (amount) = (?)
                    WHERE recipe = ? AND ingredient = ?;
                    """;
        try (var conn = connections.get();
             var preparedStatement =
                     conn.use().prepareStatement(query)) {
            for (var entry : recipeValueObject.ingredients().entrySet()) {
                preparedStatement.setDouble(1, entry.getValue());
                preparedStatement.setString(2, recipeValueObject.name());
                preparedStatement.setString(3, entry.getKey());
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();

        } catch (SQLException e) {
            throw new DataStorageException("Failed to update recipe ingredients in database", e);
        }
    }

    private RecipeValueObject getRecipeWithIngredients(ResultSet resultSet) throws SQLException {
        var tempRecipeDto = getRecipeFromResultSet(resultSet);
        var ingredientsOfRecipe = getIngredientsOfRecipe(tempRecipeDto);
        return new RecipeValueObject(
                tempRecipeDto.name(),
                tempRecipeDto.category(),
                ingredientsOfRecipe,
                tempRecipeDto.estimatedDuration(),
                tempRecipeDto.portions(),
                tempRecipeDto.description(),
                tempRecipeDto.instruction()
        );
    }

    private Map<String, Double> getIngredientsOfRecipe(RecipeValueObject recipeValueObject) {
        String query = """
                    SELECT recipe, ingredient, amount
                    FROM RecipeIngredients
                    WHERE recipe = ?;
                    """;
        try (var conn = connections.get();
             var preparedStatement =
                     conn.use().prepareStatement(query)) {

            preparedStatement.setString(1, recipeValueObject.name());

            Map<String, Double> ingredients = new HashMap<>();
            try (var resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    var recipe = resultSet.getString(1);
                    var ingredient = resultSet.getString(2);
                    var amount = resultSet.getDouble(3);
                    ingredients.put(ingredient, amount);
                }
            }

            return Map.copyOf(ingredients);

        } catch (SQLException e) {
            throw new DataStorageException("Could not read from database", e);
        }
    }

    private RecipeValueObject getRecipeFromResultSet(ResultSet resultSet) throws SQLException {
        return new RecipeValueObject(
                resultSet.getString(1),
                resultSet.getString(2),
                null,
                resultSet.getInt(3),
                resultSet.getInt(4),
                resultSet.getString(5),
                resultSet.getString(6)
        );
    }
}
