package cz.muni.fi.pv168.recipesdb.ui.filter;

public enum ComboBoxFilterType {
    RECIPE_CATEGORY,
    RECIPE_INGREDIENT
}
