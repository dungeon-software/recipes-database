package cz.muni.fi.pv168.recipesdb.data.manipulation;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import cz.muni.fi.pv168.recipesdb.model.Recipe;

import cz.muni.fi.pv168.recipesdb.util.Formatter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Collection;

/**
 * @author Vít Šebela
 */
public class RecipePdfExporter implements Exporter {
    @Override
    public void exportRecipes(Collection<Recipe> recipes, String filePath) {

        Document document = new Document();
        try {
            PdfWriter.getInstance(document, new FileOutputStream(filePath));
            document.open();
            addMetaData(document);
            Paragraph paragraph = new Paragraph();

            Font font = FontFactory.getFont(FontFactory.TIMES, 12, BaseColor.BLACK);
            for (var recipe : recipes) {
                paragraph.add(new Paragraph(Formatter.recipePDFRepresentation(recipe), font));
                paragraph.add(new Paragraph(" "));
                document.newPage();
            }
            document.add(paragraph);
            document.close();

        } catch (DocumentException e) {
            throw new DataManipulationException("Could not write to PDF file",e);
        } catch (FileNotFoundException e) {
            throw new DataManipulationException("Could not open file to write", e);
        }
    }

    private void addMetaData(Document document) {
        document.addTitle("RecipesDB PDF");
        document.addAuthor("Vít Šebela");
        document.addCreator("Vít Šebela");
    }
}
