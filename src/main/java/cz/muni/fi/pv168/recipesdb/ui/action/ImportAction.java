package cz.muni.fi.pv168.recipesdb.ui.action;

import cz.muni.fi.pv168.recipesdb.data.manipulation.Importer;
import cz.muni.fi.pv168.recipesdb.model.Recipe;
import cz.muni.fi.pv168.recipesdb.ui.model.GenericTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Collection;
import java.util.Objects;
import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.WindowConstants;

/**
 * @author Vít Šebela
 */
public final class ImportAction extends AbstractAction {
    private final JTable table;
    private final Importer importer;

    public ImportAction(JTable table, Importer importer) {
        super("Import");
        this.table = Objects.requireNonNull(table);
        this.importer = Objects.requireNonNull(importer);
        putValue(SHORT_DESCRIPTION, "Imports recipes from JSON a file");
        putValue(MNEMONIC_KEY, KeyEvent.VK_I);
        putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl I"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        var fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));

        int dialogResult = fileChooser.showOpenDialog(table);
        if (dialogResult == JFileChooser.APPROVE_OPTION) {
            File importFile = fileChooser.getSelectedFile();
            Collection<Recipe> importedRecipes = importer.importRecords(importFile.getAbsolutePath());

            if (importedRecipes.size() == 0) {
                var frame = new JFrame("Import status");
                frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

                JOptionPane.showMessageDialog(frame,
                    "No recipes were imported!\n" +
                            "Reason: non-existent categories or ingredients!",
                    "Import status", JOptionPane.PLAIN_MESSAGE);
                return;
            }

            var recipeTableModel = (GenericTableModel) table.getModel();
            importedRecipes.forEach(recipeTableModel::addRow);
        }
    }
}
