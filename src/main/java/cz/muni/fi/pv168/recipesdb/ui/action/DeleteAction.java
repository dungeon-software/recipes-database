package cz.muni.fi.pv168.recipesdb.ui.action;

import cz.muni.fi.pv168.recipesdb.data.storage.DataStorageException;
import cz.muni.fi.pv168.recipesdb.ui.model.GenericTableModel;
import cz.muni.fi.pv168.recipesdb.util.Notificator;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.Arrays;
import java.util.Comparator;
import javax.swing.AbstractAction;
import javax.swing.JTable;
import javax.swing.KeyStroke;

public class DeleteAction extends AbstractAction {

    private final JTable table;

    public DeleteAction(JTable table) {
        super("Delete"); //, Icons.DELETE_ICON
        this.table = table;
        putValue(SHORT_DESCRIPTION, "Deletes selected records");
        putValue(MNEMONIC_KEY, KeyEvent.VK_D);
        putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl D"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        var tableModel = (GenericTableModel) table.getModel();
        try {
            Arrays.stream(table.getSelectedRows())
                    // view row index must be converted to model row index
                    .map(table::convertRowIndexToModel)
                    .boxed()
                    // We need to delete rows in descending order to not change index of rows
                    // which are not deleted yet
                    .sorted(Comparator.reverseOrder())
                    .forEach(tableModel::deleteRow);
        } catch (DataStorageException exception) {
            Notificator.showInvalidReferentialIntegrityDelete();
        }
    }
}
