package cz.muni.fi.pv168.recipesdb.data;

import cz.muni.fi.pv168.recipesdb.model.Category;
import cz.muni.fi.pv168.recipesdb.model.CategoryColors;
import cz.muni.fi.pv168.recipesdb.model.Ingredient;
import cz.muni.fi.pv168.recipesdb.model.Unit;
import cz.muni.fi.pv168.recipesdb.ui.controller.TableCreator;
import cz.muni.fi.pv168.recipesdb.ui.model.GenericListModel;
import cz.muni.fi.pv168.recipesdb.wiring.DependencyProvider;
import cz.muni.fi.pv168.recipesdb.wiring.ProductionDependencyProvider;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.swing.JTable;

public class DataAccess {
    private static final DependencyProvider dependencyProvider = new ProductionDependencyProvider();
    private static final JTable recipeTable = TableCreator.createRecipeTable(dependencyProvider.getRecipeRepository());
    private static final JTable categoryTable = TableCreator.createCategoryTable(dependencyProvider.getCategoryRepository());
    private static final JTable ingredientTable = TableCreator.createIngredientTable(dependencyProvider.getIngredientRepository());
    private static final JTable unitTable = TableCreator.createUnitTable(dependencyProvider.getUnitRepository());
    private static Map<String, Unit> baseUnits;

    public static JTable getRecipeTable() {
        return recipeTable;
    }

    public static JTable getCategoryTable() {
        return categoryTable;
    }

    public static JTable getIngredientTable() {
        return ingredientTable;
    }

    public static JTable getUnitTable() {
        return unitTable;
    }

    public static void wipeRecipes() {
        dependencyProvider.getRecipeRepository().findAll()
                .forEach(r -> dependencyProvider.getRecipeRepository().deleteByName(r.getName()));
    }

    public static DependencyProvider getDependencyProvider() {
        return dependencyProvider;
    }

    public static List<Category> getCategoryList() {
        return dependencyProvider.getCategoryRepository().findAll();
    }

    public static GenericListModel<Category> getCategoryListModel() {
        return new GenericListModel<>(getCategoryList());
    }

    public static List<CategoryColors> getCategoryColorsList() {
        return Stream.of(CategoryColors.values()).collect(Collectors.toList());
    }

    public static GenericListModel<CategoryColors> getCategoryColorsListModel() {
        return new GenericListModel<>(getCategoryColorsList());
    }

    public static List<Ingredient> getIngredientList() {
        return dependencyProvider.getIngredientRepository().findAll();
    }

    public static GenericListModel<Ingredient> getIngredientListModel() {
        return new GenericListModel<>(getIngredientList());
    }

    public static List<Unit> getUnitList() {
        return dependencyProvider.getUnitRepository().findAll();
    }

    public static GenericListModel<Unit> getUnitListModel() {
        return new GenericListModel<>(getUnitList());
    }

    public static Unit getBaseUnit(String name) {
        if (getBaseUnits().containsKey(name)) {
            return getBaseUnits().get(name);
        } else {
            throw new IllegalArgumentException("Given unit name doesn't match any basic unit!");
        }
    }

    public static Map<String, Unit> getBaseUnits() {
        if (baseUnits == null) {
            baseUnits = Map.of(
                    "KG", new Unit("kilogram", "kg", null),
                    "G", new Unit("gram", "g", null),
                    "ML", new Unit("mililiter", "ml", null),
                    "L", new Unit("liter", "l", null)
            );
        }
        return baseUnits;
    }

    public static List<Unit> getBaseUnitList() {
        return getBaseUnits().values().stream().toList();
    }

    public static GenericListModel<Unit> getBaseUnitListModel() {
        return new GenericListModel<>(getBaseUnitList());
    }

    public static GenericListModel<Unit> getUnitListModel(Unit unit) {
        List<Unit> unitList = new ArrayList<>(getUnitList());
        unitList.remove(unit);
        return new GenericListModel<>(unitList);
    }
}
