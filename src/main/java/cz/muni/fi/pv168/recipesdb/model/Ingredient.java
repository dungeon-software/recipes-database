package cz.muni.fi.pv168.recipesdb.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

/**
 * Class representing a recipe ingredient
 *
 * @author Stanislav Zeman
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Ingredient implements TableEntity {
    @NonNull
    private String name;
    @NonNull
    private Unit unit;
    @NonNull
    private Integer nutritionalValue;

    @Override
    public String toString() {
        return getName();
    }
}
