package cz.muni.fi.pv168.recipesdb.ui.action;

import cz.muni.fi.pv168.recipesdb.data.storage.DataStorageException;
import cz.muni.fi.pv168.recipesdb.ui.dialog.EntityDialog;
import cz.muni.fi.pv168.recipesdb.ui.model.GenericTableModel;
import cz.muni.fi.pv168.recipesdb.util.Notificator;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.function.Supplier;
import javax.swing.AbstractAction;
import javax.swing.JTable;
import javax.swing.KeyStroke;

/**
 * Class representing add action on a given UI table
 *
 * @param <T> Type of records stored in given the table
 * @author Vít Šebela
 * @author Stanislav Zeman
 */
public class AddAction<T> extends AbstractAction {
    private final JTable table;
    private final String description;
    private final Supplier<EntityDialog<T>> createDialog;

    public AddAction(JTable table, Supplier<EntityDialog<T>> createDialog) {
        this(table, "Add", "Adds a new record", createDialog);
    }

    public AddAction(JTable table, String name, String description, Supplier<EntityDialog<T>> createDialog) {
        super(name);
        this.table = table;
        this.description = description;
        this.createDialog = createDialog;

        putValue(SHORT_DESCRIPTION, description);
        putValue(MNEMONIC_KEY, KeyEvent.VK_A);
        putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl N"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        var tableModel = (GenericTableModel<T>) table.getModel();

        var dialog = createDialog.get();

        try {
            dialog.show(table, description).ifPresent(tableModel::addRow);
        } catch (DataStorageException dse) {
            Notificator.showObjectNameAlreadyExistsNotification();
        }
    }
}
