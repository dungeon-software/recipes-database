package cz.muni.fi.pv168.recipesdb.data.storage.repository;

import cz.muni.fi.pv168.recipesdb.data.storage.dao.IngredientDao;
import cz.muni.fi.pv168.recipesdb.data.storage.valueobject.IngredientValueObject;
import cz.muni.fi.pv168.recipesdb.data.storage.mapper.EntityMapper;
import cz.muni.fi.pv168.recipesdb.model.Ingredient;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Vít Šebela
 */
public class IngredientRepository implements Repository<Ingredient>{
    private final IngredientDao dao;
    private final EntityMapper<IngredientValueObject, Ingredient> mapper;
    private List<Ingredient> ingredients;

    public IngredientRepository(IngredientDao dao, EntityMapper<IngredientValueObject, Ingredient> mapper) {
        this.dao = dao;
        this.mapper = mapper;
        this.ingredients = new ArrayList<>();
        refresh();
    }

    @Override
    public int getSize() {
        return ingredients.size();
    }

    @Override
    public Optional<Ingredient> findByName(String name) {
        return ingredients.stream()
                .filter(ingredient -> ingredient.getName().equals(name))
                .findFirst();
    }

    @Override
    public Optional<Ingredient> findByIndex(int index) {
        if (index < getSize()) {
            return Optional.of(ingredients.get(index));
        }
        return Optional.empty();
    }

    @Override
    public List<Ingredient> findAll() {
        return List.copyOf(ingredients);
    }

    @Override
    public void refresh() {
        ingredients = dao.findAll().stream()
                .map(x -> mapper.mapToModel(x))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    @Override
    public void create(Ingredient newEntity) {
        dao.create(mapper.mapToDatabaseEntity(newEntity));
        refresh();
    }

    @Override
    public void update(Ingredient entity) {
        dao.update(mapper.mapToDatabaseEntity(entity));
        refresh();
    }

    @Override
    public void deleteByName(String name) {
        dao.deleteByName(name);
        refresh();
    }
}
