package cz.muni.fi.pv168.recipesdb.data.storage.valueobject;

/**
 * @author Vít Šebela
 */
public record RecipeIngredientValueObject(String recipe, String ingredient, int amount){
    public RecipeIngredientValueObject(String recipe, String ingredient, int amount) {
        this.recipe = recipe;
        this.ingredient = ingredient;
        this.amount = amount;
    }
}
