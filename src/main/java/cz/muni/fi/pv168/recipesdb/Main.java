package cz.muni.fi.pv168.recipesdb;

import com.formdev.flatlaf.FlatLightLaf;
import cz.muni.fi.pv168.recipesdb.ui.component.MainWindow;
import java.awt.EventQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;

public class Main {

    public static void main(String[] args) {

        initFlatLightLafLookAndFeel();
        EventQueue.invokeLater(() -> new MainWindow().show());
    }

    private static void initFlatLightLafLookAndFeel() {
        try {
            UIManager.setLookAndFeel(new FlatLightLaf());
        } catch (Exception ex) {
            Logger.getLogger("Main")
                    .log(Level.SEVERE, "FlatLightLaf look and feel initialization failed", ex);
        }
    }
}
