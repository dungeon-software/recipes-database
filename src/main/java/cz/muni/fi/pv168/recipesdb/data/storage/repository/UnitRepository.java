package cz.muni.fi.pv168.recipesdb.data.storage.repository;

import cz.muni.fi.pv168.recipesdb.data.storage.dao.UnitDao;
import cz.muni.fi.pv168.recipesdb.data.storage.valueobject.UnitValueObject;
import cz.muni.fi.pv168.recipesdb.data.storage.mapper.EntityMapper;
import cz.muni.fi.pv168.recipesdb.model.Unit;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Vít Šebela
 */
public class UnitRepository implements Repository<Unit>{

    private final UnitDao dao;
    private final EntityMapper<UnitValueObject, Unit> mapper;
    private List<Unit> units;

    public UnitRepository(UnitDao dao, EntityMapper<UnitValueObject, Unit> mapper) {
        this.dao = dao;
        this.mapper = mapper;
        this.units = new ArrayList<>();
        refresh();
    }

    @Override
    public int getSize() {
        return units.size();
    }

    @Override
    public Optional<Unit> findByName(String name) {
        return units.stream()
                .filter(unit ->
                        unit
                        .getName()
                        .equals(name))
                .findFirst();
    }

    @Override
    public Optional<Unit> findByIndex(int index) {
        if (index < getSize()) {
            return Optional.of(units.get(index));
        }
        return Optional.empty();
    }

    @Override
    public List<Unit> findAll() {
        return List.copyOf(units);
    }

    @Override
    public void refresh() {
        units = dao.findAll().stream()
                .map(mapper::mapToModel)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    @Override
    public void create(Unit newEntity) {
        dao.create(mapper.mapToDatabaseEntity(newEntity));
        refresh();
    }

    @Override
    public void update(Unit entity) {
        dao.update(mapper.mapToDatabaseEntity(entity));
        refresh();
    }

    @Override
    public void deleteByName(String name) {
        dao.deleteByName(name);
        refresh();
    }
}
