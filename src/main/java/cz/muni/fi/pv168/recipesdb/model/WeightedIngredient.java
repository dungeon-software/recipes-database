package cz.muni.fi.pv168.recipesdb.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;

/**
 * Class representing an ingredient which has an amount linked to it
 *
 * @author Stanislav Zeman
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WeightedIngredient implements TableEntity {
    @NonNull
    private Ingredient ingredient;
    @NonNull
    @EqualsAndHashCode.Exclude
    private Double amount;

    public WeightedIngredient(@NonNull Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    @Override
    public String getName() {
        return ingredient.getName();
    }
}
