package cz.muni.fi.pv168.recipesdb.data.storage.database;

import java.io.Closeable;
import java.sql.Connection;

/**
 * @author Vít Šebela
 */
public interface ConnectionHandler extends Closeable {

    /**
     * @return {@link Connection} instance managed by this handler
     */
    Connection use();

    /**
     * Closes managed {@link Connection} when desired
     */
    void close();
}
