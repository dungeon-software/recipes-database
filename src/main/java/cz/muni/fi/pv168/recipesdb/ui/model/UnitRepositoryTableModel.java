package cz.muni.fi.pv168.recipesdb.ui.model;

import cz.muni.fi.pv168.recipesdb.data.storage.repository.Repository;
import cz.muni.fi.pv168.recipesdb.model.TableEntity;
import cz.muni.fi.pv168.recipesdb.model.Unit;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Stanislav Zeman
 * @since 23.11.2022
 */
public class UnitRepositoryTableModel extends GenericRepositoryTableModel<Unit> {
    public UnitRepositoryTableModel(Repository<Unit> repository, List<Column<Unit, ?>> columns) {
        super(repository, columns);
    }

    @Override
    public int getRowCount() {
        return getRecords().size();
    }

    @Override
    public List<Unit> getRecords() {
        return super.getRecords().stream()
                .filter(Unit::isNotBaseUnit)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteRow(int rowIndex) {
        if (rowIndex >= getRowCount()) {
            throw new ArrayIndexOutOfBoundsException("Row record with index: " + rowIndex + " doesn't exist");
        }
        TableEntity unwrapped = getRecords().get(rowIndex);
        super.getRepository().deleteByName(unwrapped.getName());
        fireTableRowsDeleted(rowIndex, rowIndex);
    }

    @Override
    public void addRow(Unit entity) {
        int index = getRowCount();
        super.getRepository().create(entity);
        fireTableRowsInserted(index, index);
    }

    @Override
    public Unit getEntity(int rowIndex) {
        if (rowIndex >= getRowCount()) {
            throw new ArrayIndexOutOfBoundsException("Row record with index: " + rowIndex + " doesn't exist");
        }
        return getRecords().get(rowIndex);
    }
}
