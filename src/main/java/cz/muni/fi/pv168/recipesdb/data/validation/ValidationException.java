package cz.muni.fi.pv168.recipesdb.data.validation;

import cz.muni.fi.pv168.recipesdb.error.RuntimeApplicationException;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ValidationException extends RuntimeApplicationException {
    private final List<String> validationErrors;

    public ValidationException(String userMessage, String message, List<String> validationErrors) {
        super(userMessage, message);
        this.validationErrors = validationErrors;
        Logger.getLogger("Data-validation")
                .log(Level.WARNING, message + String.join("; ", validationErrors));
    }

    public ValidationException(String message, List<String> validationErrors) {
        this("Validation failed", message, validationErrors);
    }

    public List<String> getValidationErrors() {
        return Collections.unmodifiableList(validationErrors);
    }
}
