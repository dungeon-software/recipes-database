package cz.muni.fi.pv168.recipesdb.data.storage.dao;

import cz.muni.fi.pv168.recipesdb.data.storage.DataStorageException;
import cz.muni.fi.pv168.recipesdb.data.storage.database.ConnectionHandler;
import cz.muni.fi.pv168.recipesdb.data.storage.valueobject.UnitValueObject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * @author Vít Šebela
 */
public class UnitDao implements Dao<UnitValueObject>{
    private final Supplier<ConnectionHandler> connections;

    public UnitDao(Supplier<ConnectionHandler> connections) {
        this.connections = connections;
    }

    @Override
    public UnitValueObject create(UnitValueObject unit) {
        String query = """
            INSERT INTO Unit (name, acronym, amount, baseUnit)
            VALUES (?,?,?,?);
            """;
        try (var conn = connections.get();
            var preparedStatement = conn.use().prepareStatement(query)){
            preparedStatement.setString(1, unit.name());
            preparedStatement.setString(2, unit.acronym());
            preparedStatement.setDouble(3, unit.amount());
            preparedStatement.setString(4, unit.baseUnit());

            preparedStatement.executeUpdate();

            return findByName(unit.name()).orElseThrow();

        } catch (SQLException e) {
            throw new DataStorageException("Failed to create new unit",e);
        }
    }

    @Override
    public Collection<UnitValueObject> findAll() {
        String query = """
            SELECT name, acronym, amount, baseUnit
            FROM Unit
            """;
        try (var conn = connections.get();
             var preparedStatement =
                     conn.use().prepareStatement(query)){

            List<UnitValueObject> units = new ArrayList<>();
            try (var resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    var unit = getUnitsFromResultSet(resultSet);
                    units.add(unit);
                }
            }

            return List.copyOf(units);

        } catch (SQLException e) {
            throw new DataStorageException("Could not read from database",e);
        }
    }

    @Override
    public Optional<UnitValueObject> findByName(String name) {
        String query = """
            SELECT name, acronym, amount, baseUnit
            FROM Unit
            WHERE name = ?;
            """;
        try (var conn = connections.get();
             var preparedStatement =
                     conn.use().prepareStatement(query)){

            preparedStatement.setString(1,name);
            try (var resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(getUnitsFromResultSet(resultSet));
                }
                return Optional.empty();
            }

        } catch (SQLException e) {
            throw new DataStorageException("Could not read from database",e);
        }
    }

    @Override
    public UnitValueObject update(UnitValueObject unit) {
        String query = """
            UPDATE Unit
            SET (acronym, amount, baseUnit) = (?,?,?)
            WHERE name = ?;
            """;
        try (var conn = connections.get();
             var preparedStatement =
                     conn.use().prepareStatement(query)){

            preparedStatement.setString(1,unit.acronym());
            preparedStatement.setDouble(2,unit.amount());
            preparedStatement.setString(3, unit.baseUnit());
            preparedStatement.setString(4, unit.name());
            preparedStatement.executeUpdate();

            return findByName(unit.name()).orElseThrow();

        } catch (SQLException e) {
            throw new DataStorageException("Failed to update unit in database",e);
        }
    }

    @Override
    public void deleteByName(String name) {
        String query = """
            DELETE FROM Unit
            WHERE name = ?
            """;
        try (var conn = connections.get();
             var preparedStatement =
                     conn.use().prepareStatement(query)){

            preparedStatement.setString(1,name);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DataStorageException("Could not delete unit by name",e);
        }
    }

    private UnitValueObject getUnitsFromResultSet(ResultSet resultSet) throws SQLException{
        return new UnitValueObject(
                resultSet.getString("name"),
                resultSet.getString("acronym"),
                resultSet.getDouble("amount"),
                resultSet.getString("baseUnit")
        );
    }
}
