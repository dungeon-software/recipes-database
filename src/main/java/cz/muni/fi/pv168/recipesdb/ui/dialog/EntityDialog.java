package cz.muni.fi.pv168.recipesdb.ui.dialog;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;

import java.util.Optional;

import static javax.swing.JOptionPane.OK_CANCEL_OPTION;
import static javax.swing.JOptionPane.OK_OPTION;
import static javax.swing.JOptionPane.PLAIN_MESSAGE;

public abstract class EntityDialog<E> implements ShowableDialog{

    private final JPanel panel = new JPanel();

    EntityDialog() {
        panel.setLayout(new MigLayout("wrap 1"));
    }

    void add(String labelText, JComponent component) {
        var label = new JLabel(labelText);
        panel.add(label, "split 2, left");
        panel.add(component, "wmin 50lp, grow");
    }

    void add(String labelText) {
        var label = new JLabel(labelText);
        panel.add(label);
    }

    void add(String labelText, JComponent component, String labelText_after) {
        var label = new JLabel(labelText);
        panel.add(label, "split 3, left");
        panel.add(component, "wmin 50lp, grow");
        var label2 = new JLabel(labelText_after);
        panel.add(label2);
    }

    void add(String labelText, JComponent component, JComponent component_sec) {
        var label = new JLabel(labelText);
        panel.add(label, "split 3, left");
        panel.add(component, "wmin 50lp, grow");
        panel.add(component_sec, "wmin 50lp, grow");
    }

    void add(JTextField textField) {
        panel.add(textField);
    }

    void add(JTable table) {
        panel.add(table, "wmin 250lp, hmin 250lp, grow");
    }

    abstract E getEntity();

    public Optional<E> show(JComponent parentComponent, String title) {
        int result = JOptionPane.showOptionDialog(parentComponent, panel, title,
                OK_CANCEL_OPTION, PLAIN_MESSAGE, null, null, null);
        if (result == OK_OPTION) {
            return Optional.of(getEntity());
        } else {
            return Optional.empty();
        }
    }

    public void createTextArea(JTextArea textArea){
        JScrollPane textScroll = new JScrollPane(textArea,    JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        panel.add(textScroll,"grow,span,wrap");
    }

    @Override
    public Class getDialogClassType() {
        return this.getClass();
    }
}
