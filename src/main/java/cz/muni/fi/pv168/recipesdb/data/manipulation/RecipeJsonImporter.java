package cz.muni.fi.pv168.recipesdb.data.manipulation;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import cz.muni.fi.pv168.recipesdb.data.DataAccess;
import cz.muni.fi.pv168.recipesdb.data.storage.repository.Repository;
import cz.muni.fi.pv168.recipesdb.model.Recipe;
import cz.muni.fi.pv168.recipesdb.util.Notificator;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.stream.Collectors;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;

/**
 * @author Vít Šebela
 */
public class RecipeJsonImporter implements Importer {
    private final Repository<Recipe> recipeRepository;
    private final Gson gson;

    public RecipeJsonImporter(Repository<Recipe> recipeRepository) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        gsonBuilder.setLenient();
        this.gson = gsonBuilder.create();
        this.recipeRepository = recipeRepository;
    }

    @Override
    public Collection<Recipe> importRecords(String filePath) {

        if (!recipeRepository.findAll().isEmpty()) {
            var frame = new JFrame("Continue with import");
            frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

            var option = JOptionPane.showConfirmDialog(frame,
                    "Recipe database is not empty!\n" +
                            "Wipe all recipes and continue?",
                    "Continue with import", JOptionPane.YES_NO_OPTION);
            if (option != 0) { // 0 == YES, everything otherwise NO
                return Collections.emptyList();
            }
            DataAccess.wipeRecipes();
        }

        try (var reader = new BufferedReader(new FileReader(filePath, StandardCharsets.UTF_8))) {
            String importedData = reader.lines()
                    .collect(Collectors.joining());
            Recipe[] recipeArray = gson.fromJson(importedData, Recipe[].class);
            return Arrays.stream(recipeArray)
                    .filter(recipe -> DataAccess.getDependencyProvider().getCategoryRepository().findAll()
                            .contains(recipe.getCategory()))
                    .filter(recipe -> new HashSet<>(DataAccess.getDependencyProvider().getIngredientRepository().findAll())
                            .containsAll(recipe.getNonWeightedIngredients()))
                    .toList();

        } catch (FileNotFoundException exception) {
            Notificator.showErrorDialog("File does not exist!");
            throw new DataManipulationException("File does not exist", exception);
        } catch (IOException exception) {
            Notificator.showErrorDialog("Unable to read from file!");
            throw new DataManipulationException("Unable to read file", exception);
        } catch (JsonSyntaxException exception) {
            Notificator.showInvalidJsonFile(exception.getStackTrace()[0].getLineNumber());
            throw new DataManipulationException("JSON file is not valid ", exception);
        }
    }
}
