package cz.muni.fi.pv168.recipesdb.data.manipulation;

import cz.muni.fi.pv168.recipesdb.model.Recipe;

import java.util.Collection;

public interface Importer {
    /**
     * Imports records from a file to an ordered {@link Collection}.
     *
     * @param filePath absolute path of the file to import
     *
     * @return imported records (in the same order as in the file)
     *
     * @throws DataManipulationException if the file to import does not exist,
     *         cannot be read or its format/encoding is invalid
     */
    Collection<Recipe> importRecords(String filePath);
}
