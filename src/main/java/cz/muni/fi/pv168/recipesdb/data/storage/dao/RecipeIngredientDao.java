package cz.muni.fi.pv168.recipesdb.data.storage.dao;

import cz.muni.fi.pv168.recipesdb.data.storage.DataStorageException;
import cz.muni.fi.pv168.recipesdb.data.storage.database.ConnectionHandler;
import cz.muni.fi.pv168.recipesdb.data.storage.valueobject.RecipeIngredientValueObject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * @author Vít Šebela
 */
public class RecipeIngredientDao implements Dao<RecipeIngredientValueObject>{
    private final Supplier<ConnectionHandler> connections;

    public RecipeIngredientDao(Supplier<ConnectionHandler> connections) {
        this.connections = connections;
    }

    @Override
    public RecipeIngredientValueObject create(RecipeIngredientValueObject entity) {
        String query = """
                    INSERT INTO RecipeIngredients (recipe, ingredient, amount)
                    VALUES (?,?,?);
                    """;
        try (var conn = connections.get();
             var preparedStatement = conn.use().prepareStatement(query)) {
            preparedStatement.setString(1,entity.recipe());
            preparedStatement.setString(2,entity.ingredient());
            preparedStatement.setInt(3,entity.amount());

            preparedStatement.executeUpdate();
            return findAll().stream()
                    .filter(i -> i.recipe().equals(entity.recipe()))
                    .filter(i -> i.ingredient().equals(entity.ingredient()))
                    .findFirst()
                    .orElseThrow();

        } catch (SQLException e) {
            throw new DataStorageException("Failed to create ingredient of recipe", e);
        }
    }

    @Override
    public Collection<RecipeIngredientValueObject> findAll() {
        String query = """
                    SELECT recipe, ingredient, amount
                    FROM RecipeIngredients;
                    """;
        try (var conn = connections.get();
             var preparedStatement = conn.use().prepareStatement(query)) {

            List<RecipeIngredientValueObject> ingredients = new ArrayList<>();
            try (var resultSet = preparedStatement.executeQuery()){
                while (resultSet.next()) {
                    ingredients.add(getIngredientsFromResultSet(resultSet));
                }
            }
            return List.copyOf(ingredients);

        } catch (SQLException e) {
            throw new DataStorageException("Failed to retrieve ingredients of recipe", e);
        }
    }

    @Override
    public Optional<RecipeIngredientValueObject> findByName(String name) {
        return Optional.empty();
    }

    public Optional<RecipeIngredientValueObject> find(String recipe, String ingredient) {
        String query = """
                    SELECT recipe, ingredient, amount
                    FROM RecipeIngredients
                    WHERE recipe = ? and ingredient = ?;
                    """;
        try (var conn = connections.get();
             var preparedStatement = conn.use().prepareStatement(query)) {
            preparedStatement.setString(1,recipe);
            preparedStatement.setString(1,ingredient);

            try (var resultSet = preparedStatement.executeQuery()){
                return Optional.of(getIngredientsFromResultSet(resultSet));
            }

        } catch (SQLException e) {
            throw new DataStorageException("Failed to retrieve ingredients of recipe", e);
        }
    }

    public Collection<RecipeIngredientValueObject> findByRecipe(String recipe) {
        String query = """
                    SELECT recipe, ingredient, amount
                    FROM RecipeIngredients
                    WHERE recipe = ?;
                    """;
        try (var conn = connections.get();
             var preparedStatement = conn.use().prepareStatement(query)) {
            preparedStatement.setString(1,recipe);

            List<RecipeIngredientValueObject> ingredients = new ArrayList<>();
            try (var resultSet = preparedStatement.executeQuery()){
                while (resultSet.next()) {
                    ingredients.add(getIngredientsFromResultSet(resultSet));
                }
                return List.copyOf(ingredients);
            }

        } catch (SQLException e) {
            throw new DataStorageException("Failed to retrieve ingredients of recipe", e);
        }
    }

    @Override
    public RecipeIngredientValueObject update(RecipeIngredientValueObject entity) {
        String query = """
                    UPDATE RecipeIngredients
                    SET (amount) = (?)
                    WHERE recipe = ? AND ingredient = ?;
                    """;
        try (var conn = connections.get();
             var preparedStatement = conn.use().prepareStatement(query)) {
            preparedStatement.setInt(1,entity.amount());
            preparedStatement.setString(2,entity.recipe());
            preparedStatement.setString(3,entity.ingredient());

            preparedStatement.executeUpdate();

            return findAll().stream()
                    .filter(i -> i.recipe().equals(entity.recipe()))
                    .filter(i -> i.ingredient().equals(entity.ingredient()))
                    .findFirst()
                    .orElseThrow();

        } catch (SQLException e) {
            throw new DataStorageException("Failed to update ingredients of recipe", e);
        }
    }

    @Override
    public void deleteByName(String name) {
        return;
    }

    public void deleteByName(String recipe, String ingredient) {
        String query = """
                    DELETE FROM RecipeIngredients
                    WHERE recipe = ? AND ingredient = ?
                    """;
        try (var conn = connections.get();
             var preparedStatement =
                     conn.use().prepareStatement(query)) {
            preparedStatement.setString(1, recipe);
            preparedStatement.setString(2, ingredient);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DataStorageException("Could not delete from database", e);
        }
    }

    private RecipeIngredientValueObject getIngredientsFromResultSet(ResultSet resultSet) throws SQLException {
        return new RecipeIngredientValueObject(
                resultSet.getString(1),
                resultSet.getString(2),
                resultSet.getInt(3)
        );
    }
}
