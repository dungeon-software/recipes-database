package cz.muni.fi.pv168.recipesdb.data.storage.repository;

import cz.muni.fi.pv168.recipesdb.data.storage.dao.RecipeDao;
import cz.muni.fi.pv168.recipesdb.data.storage.dao.RecipeIngredientDao;
import cz.muni.fi.pv168.recipesdb.data.storage.mapper.RecipeMapper;
import cz.muni.fi.pv168.recipesdb.model.Ingredient;
import cz.muni.fi.pv168.recipesdb.model.Recipe;
import cz.muni.fi.pv168.recipesdb.model.WeightedIngredient;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Vít Šebela
 */
public class RecipeRepository implements Repository<Recipe> {
    private final RecipeDao recipeDao;
    private final RecipeMapper recipeMapper;
    private List<Recipe> recipes;

    public RecipeRepository(RecipeDao recipeDao,
                            RecipeMapper recipeMapper) {
        this.recipeDao = recipeDao;
        this.recipeMapper = recipeMapper;
        this.recipes = new ArrayList<>();
        refresh();
    }

    @Override
    public int getSize() {
        return recipes.size();
    }

    @Override
    public Optional<Recipe> findByName(String name) {
        return recipes.stream()
                .filter(recipe -> recipe.getName().equals(name))
                .findFirst();
    }

    @Override
    public Optional<Recipe> findByIndex(int index) {
        if (index < getSize()) {
            return Optional.of(recipes.get(index));
        }
        return Optional.empty();
    }

    @Override
    public List<Recipe> findAll() {
        return List.copyOf(recipes);
    }

    @Override
    public void refresh() {
        recipes = recipeDao.findAll().stream()
                .map(recipeMapper::mapToModel)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    @Override
    public void create(Recipe newEntity) {
        recipeDao.create(recipeMapper.mapToDatabaseEntity(newEntity));
        refresh();
    }

    @Override
    public void update(Recipe entity) {
        recipeDao.deleteIngredientsByRecipe(entity.getName());
        recipeDao.createIngredientsForRecipe(recipeMapper.mapToDatabaseEntity(entity));
        recipeDao.update(recipeMapper.mapToDatabaseEntity(entity));
        refresh();
    }

    @Override
    public void deleteByName(String name) {
        recipeDao.deleteByName(name);
        refresh();
    }

    public void deleteIngredientByName(String recipe, String ingredient) {
        recipeDao.deleteIngredientByName(recipe, ingredient);
        refresh();
    }
}
