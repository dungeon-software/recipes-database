package cz.muni.fi.pv168.recipesdb.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;

/**
 * Class that represents a unit in which ingredients are measured
 *
 * @author Stanislav Zeman
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Unit implements TableEntity {
    @NonNull
    private String name;
    @NonNull
    @EqualsAndHashCode.Exclude
    private String acronym;
    @EqualsAndHashCode.Exclude
    private UnitConversion unitConversion;

    public boolean isBaseUnit() {
        return equals(unitConversion.getUnit());
    }

    public boolean isNotBaseUnit() {
        return !isBaseUnit();
    }

    public int getIntegerConversionRatio() {
        return unitConversion.getAmount().intValue();
    }

    @Override
    public String toString() {
        return name + " [" + acronym + "]";
    }
}