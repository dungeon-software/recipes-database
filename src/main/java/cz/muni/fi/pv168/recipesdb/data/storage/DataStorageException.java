package cz.muni.fi.pv168.recipesdb.data.storage;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Vít Šebela
 */
public class DataStorageException extends RuntimeException {

    public DataStorageException(String message) {
        super(message);
        Logger.getLogger("Data-storage").log(Level.WARNING, message);
    }

    public DataStorageException(String message, Throwable cause) {
        super(message, cause);
        Logger.getLogger("Data-storage").log(Level.WARNING, message, cause);
    }
}
