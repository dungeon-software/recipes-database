package cz.muni.fi.pv168.recipesdb.data.storage.valueobject;

/**
 * @author Vít Šebela
 */
public record IngredientValueObject(String name, String unit, Integer nutritionalValue) {
    public IngredientValueObject(String name, String unit, Integer nutritionalValue) {
        this.name = name;
        this.unit = unit;
        this.nutritionalValue = nutritionalValue;
    }
}