package cz.muni.fi.pv168.recipesdb.model;

/**
 * @author Stanislav Zeman
 * @since 21.11.2022
 */
public interface TableEntity {
    String getName();
}
