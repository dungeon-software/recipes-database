package cz.muni.fi.pv168.recipesdb.ui.dialog;

import cz.muni.fi.pv168.recipesdb.data.DataAccess;
import cz.muni.fi.pv168.recipesdb.model.Unit;
import cz.muni.fi.pv168.recipesdb.model.UnitConversion;
import cz.muni.fi.pv168.recipesdb.ui.model.ComboBoxModelAdapter;
import cz.muni.fi.pv168.recipesdb.util.Formatter;
import cz.muni.fi.pv168.recipesdb.util.Notificator;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTextField;

public class UnitDialog extends EntityDialog<Unit> {
    private final JTextField name = new JTextField();
    private final JTextField acronym = new JTextField();
    private final JTextField amount = new JTextField();
    private final ComboBoxModel<Unit> conversionUnits = new ComboBoxModelAdapter<>(DataAccess.getBaseUnitListModel());
    private final Unit unit;

    private UnitDialog(Unit unit) {
        this.unit = unit;
        setInitialValues();
        addFields();
    }

    public static <T> UnitDialog createUnitDialog(T unit) {
        return new UnitDialog((Unit) unit);
    }

    public static UnitDialog createEmptyUnitDialog() {
        return new UnitDialog(
                new Unit("",
                        "",
                        new UnitConversion(0.0, DataAccess.getBaseUnit("KG"))
        ));
    }

    private void setInitialValues() {
        name.setText(unit.getName());
        acronym.setText(unit.getAcronym());
        amount.setText(Formatter.formatDecimal(unit.getUnitConversion().getAmount()).toString());
        conversionUnits.setSelectedItem(unit.getUnitConversion().getUnit());
    }

    private void addFields() {
        if (name.getText().equals("")) {
            add("Name:", name);
        }
        add("Acronym:", acronym);
        add("Amount:", amount, new JComboBox<>(conversionUnits));
    }

    @Override
    Unit getEntity() {
        unit.setName(name.getText());
        unit.setAcronym(acronym.getText());
        try {
            unit.setUnitConversion(
                    new UnitConversion(
                            Formatter.formatDecimal(Double.parseDouble(amount.getText().trim())),
                            (Unit) conversionUnits.getSelectedItem()));
        } catch (NumberFormatException nfe) {
            Notificator.showUnparseableNumberNotification("Conversion unit amount", "decimal");
        }
        return unit;
    }
}
