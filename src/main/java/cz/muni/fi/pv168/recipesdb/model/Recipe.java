package cz.muni.fi.pv168.recipesdb.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * Class representing a food recipe
 *
 * @author Stanislav Zeman
 */
@Data
@RequiredArgsConstructor
@NoArgsConstructor
public class Recipe implements TableEntity {
    @NonNull
    private String name;
    @NonNull
    private Category category;
    @NonNull
    private Integer estimatedDuration;
    @NonNull
    private Integer portions;
    @NonNull
    private String description;
    @NonNull
    private String instructions;
    @NonNull
    private List<WeightedIngredient> weightedIngredients;

    public void addIngredient(WeightedIngredient weightedIngredient) {
        weightedIngredients.add(weightedIngredient);
    }

    public void addIngredient(Ingredient ingredient, Double amount) {
        addIngredient(new WeightedIngredient(ingredient, amount));
    }

    public void removeIngredient(WeightedIngredient ingredient) {
        weightedIngredients.remove(ingredient);
    }

    public void removeIngredient(Ingredient ingredient) {
        removeIngredient(new WeightedIngredient(ingredient, 0.0));
    }

    public List<Ingredient> getNonWeightedIngredients() {
        return weightedIngredients.stream()
                .map(WeightedIngredient::getIngredient)
                .toList();
    }

    public Integer getNutritionalValue() {
        double calories = 0;

        for (WeightedIngredient weightedIngredient : weightedIngredients) {
            calories += weightedIngredient.getIngredient().getNutritionalValue() * weightedIngredient.getAmount();
        }
        return (int) Math.ceil(calories);
    }
}
