package cz.muni.fi.pv168.recipesdb.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

/**
 * Class used to add relative measurement to a custom unit
 *
 * @author Stanislav Zeman
 */
@Data
@AllArgsConstructor
public class UnitConversion {
    @NonNull
    private Double amount;
    @NonNull
    private Unit unit;
}
