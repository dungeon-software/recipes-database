package cz.muni.fi.pv168.recipesdb.data.manipulation;

import cz.muni.fi.pv168.recipesdb.model.Recipe;

import java.util.Collection;

/**
 * @author Vít Šebela
 */
public interface Exporter {
    /**
     * Exports recipes from a {@link Collection} to a file.
     *
     * @param recipes recipes to be exported (in the collection iteration order)
     * @param filePath absolute path of the export file (to be created or overwritten)
     *
     * @throws DataManipulationException if the export file cannot be opened or written
     */
    void exportRecipes(Collection<Recipe> recipes, String filePath);
}
