package cz.muni.fi.pv168.recipesdb.ui.action;

import cz.muni.fi.pv168.recipesdb.data.DataAccess;
import cz.muni.fi.pv168.recipesdb.ui.filter.Filter;

import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.RowFilter;
import javax.swing.table.TableRowSorter;

public class ApplyFiltersAction extends AbstractAction {
    private final List<Filter> filters;

    public ApplyFiltersAction(List<Filter> filters) {
        this.filters = filters;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (rangeFiltersAreValid()) {
            List<RowFilter<Object, Object>> rowFilters = filters.stream().map(Filter::getRowFilter).toList();
            ((TableRowSorter) DataAccess.getRecipeTable().getRowSorter()).setRowFilter(RowFilter.andFilter(rowFilters));
        }
    }

    private boolean rangeFiltersAreValid() {
        return filters.stream().allMatch(Filter::contentIsValid);
    }
}
