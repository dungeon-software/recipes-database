package cz.muni.fi.pv168.recipesdb.data.storage.mapper;

import cz.muni.fi.pv168.recipesdb.data.storage.valueobject.IngredientValueObject;
import cz.muni.fi.pv168.recipesdb.data.storage.repository.Repository;
import cz.muni.fi.pv168.recipesdb.data.validation.Validator;
import cz.muni.fi.pv168.recipesdb.model.Ingredient;
import cz.muni.fi.pv168.recipesdb.model.Unit;

/**
 * @author Vít Šebela
 */
public class IngredientMapper implements EntityMapper<IngredientValueObject, Ingredient> {

    private final Repository<Unit> unitRepository;
    private final Validator<Ingredient> ingredientValidator;

    public IngredientMapper(Repository<Unit> unitRepository, Validator<Ingredient> ingredientValidator) {
        this.unitRepository = unitRepository;
        this.ingredientValidator = ingredientValidator;
    }

    @Override
    public IngredientValueObject mapToDatabaseEntity(Ingredient model) {
        ingredientValidator.validate(model).intoException();

        return new IngredientValueObject(
                model.getName(),
                model.getUnit().getName(),
                model.getNutritionalValue()
        );
    }

    @Override
    public Ingredient mapToModel(IngredientValueObject entity) {
        var f = unitRepository.findAll();
        Unit unit = unitRepository.findAll().stream()
                .filter(u -> u.getName().equals(entity.unit()))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Cannot map EntityIngredient to Ingredient"));

        return new Ingredient(
                entity.name(),
                unit,
                entity.nutritionalValue()
        );
    }
}
