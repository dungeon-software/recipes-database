package cz.muni.fi.pv168.recipesdb.util;

import cz.muni.fi.pv168.recipesdb.model.Ingredient;
import cz.muni.fi.pv168.recipesdb.model.Recipe;
import cz.muni.fi.pv168.recipesdb.model.Unit;
import cz.muni.fi.pv168.recipesdb.model.UnitConversion;
import cz.muni.fi.pv168.recipesdb.model.WeightedIngredient;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

public class Formatter {
    public static Double formatDecimal(Double decimal) {
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        decimalFormat.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(Locale.ENGLISH));
        return Double.valueOf(decimalFormat.format(decimal));
    }

    public static String recipeEstimatedDurationMinutes(Recipe recipe) {
        return recipe.getEstimatedDuration() + " min";
    }

    public static String recipeNutritionalValueCalories(Recipe recipe) {
        return recipe.getNutritionalValue() + " kcal";
    }

    public static String unitConversion(Unit unit) {
        UnitConversion unitConversion = unit.getUnitConversion();

        return "= " + formatDecimal(unitConversion.getAmount()) +
                " " + unitConversion.getUnit().getName() +
                "s [" + unitConversion.getUnit().getAcronym() + "]";
    }

    public static String weightedIngredient(WeightedIngredient weightedIngredient) {
        return weightedIngredient.getIngredient().getName();
    }

    public static String weightedIngredientAmount(WeightedIngredient weightedIngredient) {
        return formatDecimal(weightedIngredient.getAmount())
                + " " + ingredientUnitWithAcronym(weightedIngredient.getIngredient());
    }

    public static String weightedIngredientAmountWithUnits(WeightedIngredient weightedIngredient) {
        Ingredient ingredient = weightedIngredient.getIngredient();
        return ingredient.getName() + ": "
                + formatDecimal(weightedIngredient.getAmount()) + " "
                + ingredient.getUnit().getAcronym();
    }

    public static String ingredientNutritionalValue(Ingredient ingredient) {
        return ingredient.getNutritionalValue() + " kcal/" + ingredient.getUnit().getAcronym();
    }

    public static String ingredientUnitWithAcronym(Ingredient ingredient) {
        return ingredient.getUnit().getName() + " [" + ingredient.getUnit().getAcronym() + "]";
    }

    public static String recipePDFRepresentation(Recipe recipe) {
        List<String> ingredients = recipe.getWeightedIngredients().stream()
                .map(Formatter::weightedIngredientAmountWithUnits)
                .toList();

        return recipe.getName() + System.lineSeparator() +
                "    Category: " + recipe.getCategory().getName() + System.lineSeparator() +
                "    Estimated duration: " + recipe.getEstimatedDuration() + System.lineSeparator() +
                "    Portions: " + recipe.getPortions() + System.lineSeparator() +
                "    Nutritional value: " + recipeNutritionalValueCalories(recipe) + System.lineSeparator() +
                "    Description: " + recipe.getDescription() + System.lineSeparator() +
                "    Instructions: " + recipe.getInstructions() + System.lineSeparator() +
                "    Ingredients: " + System.lineSeparator() +
                "    " + String.join(", ", ingredients);
    }
}
