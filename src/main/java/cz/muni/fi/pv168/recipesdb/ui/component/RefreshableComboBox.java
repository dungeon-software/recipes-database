package cz.muni.fi.pv168.recipesdb.ui.component;


import cz.muni.fi.pv168.recipesdb.data.storage.repository.Repository;

import java.util.List;
import java.util.function.Supplier;
import javax.swing.JComboBox;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

public class RefreshableComboBox<E> extends JComboBox<java.lang.Object> {
    public RefreshableComboBox(Repository<E> dataSupplier) {
        super(dataSupplier.findAll().toArray());

        addItem("All");
        setSelectedItem("All");

        addPopupMenuListener(new PopupMenuListener() {
            @Override
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                Object selectedItem = getSelectedItem();
                removeAllItems();

                JComboBox<E> comboBox = (JComboBox<E>) e.getSource();
                dataSupplier.findAll().forEach(comboBox::addItem);

                addItem("All");
                setSelectedItem(selectedItem);
            }

            @Override
            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
            }

            @Override
            public void popupMenuCanceled(PopupMenuEvent e) {
            }
        });
    }
}
