package cz.muni.fi.pv168.recipesdb.data.validation;

import cz.muni.fi.pv168.recipesdb.data.validation.common.NumberLowerBoundValidator;
import cz.muni.fi.pv168.recipesdb.data.validation.common.NumberRangeValidator;
import cz.muni.fi.pv168.recipesdb.data.validation.common.StringLengthValidator;
import cz.muni.fi.pv168.recipesdb.model.Recipe;
import java.util.List;

import static cz.muni.fi.pv168.recipesdb.data.validation.Validator.extracting;

public class RecipeValidator implements Validator<Recipe> {

    @Override
    public ValidationResult validate(Recipe model) {

        var validators = List.of(
                extracting(Recipe::getName,
                        new StringLengthValidator(1, 50, "Recipe name")),
                extracting(Recipe::getEstimatedDuration,
                        new NumberLowerBoundValidator(0, "Est. Time")),
                extracting(Recipe::getPortions,
                        new NumberLowerBoundValidator(0, "Portions")),
                extracting(Recipe::getNutritionalValue,
                        new NumberLowerBoundValidator(0, "Nutritional Value"))
        );

        return Validator.compose(validators).validate(model);
    }
}
