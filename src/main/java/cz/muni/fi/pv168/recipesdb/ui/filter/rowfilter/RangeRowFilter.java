package cz.muni.fi.pv168.recipesdb.ui.filter.rowfilter;


import cz.muni.fi.pv168.recipesdb.model.Recipe;
import cz.muni.fi.pv168.recipesdb.ui.filter.RangeFilterType;
import cz.muni.fi.pv168.recipesdb.ui.model.GenericTableModel;
import javax.swing.RowFilter;

public class RangeRowFilter extends RowFilter<Object, Object> {
    private final RangeFilterType filterType;
    private final Integer from;
    private final Integer to;

    public RangeRowFilter(RangeFilterType rangeFilterType, Integer from, Integer to) {
        this.filterType = rangeFilterType;
        this.from = from;
        this.to = to;
    }

    @Override
    public boolean include(Entry entry) {
        GenericTableModel<Recipe> tableModel = (GenericTableModel) entry.getModel();
        Recipe recipe = tableModel.getEntity((Integer) entry.getIdentifier());

        Integer value = Integer.MIN_VALUE;
        switch (filterType) {
            case RECIPE_NUTRITIONAL_VALUE -> value = recipe.getNutritionalValue();
            case RECIPE_PORTION_SIZE -> value = recipe.getPortions();
            case RECIPE_ESTIMATED_TIME -> value = recipe.getEstimatedDuration();
        }
        if (value.equals(Integer.MIN_VALUE)) {
            throw new IllegalStateException("Illegal filter value");
        }

        return value >= from && value <= to;
    }
}
