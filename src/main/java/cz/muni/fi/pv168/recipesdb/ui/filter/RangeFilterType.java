package cz.muni.fi.pv168.recipesdb.ui.filter;

public enum RangeFilterType {
    RECIPE_NUTRITIONAL_VALUE,
    RECIPE_ESTIMATED_TIME,
    RECIPE_PORTION_SIZE
}
