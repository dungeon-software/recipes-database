package cz.muni.fi.pv168.recipesdb.ui.model;

import java.util.List;

/**
 * @author Stanislav Zeman
 * @since 21.11.2022
 */
public interface GenericTableModel<T> {
    void deleteRow(int rowIndex);
    void addRow(T entity);
    void updateRow(T record);
    List<T> getRecords();
    T getEntity(int rowIndex);
}
