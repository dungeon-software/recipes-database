package cz.muni.fi.pv168.recipesdb.ui.action;

import cz.muni.fi.pv168.recipesdb.data.DataAccess;
import cz.muni.fi.pv168.recipesdb.ui.filter.Filter;
import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.table.TableRowSorter;

public class ResetFiltersAction extends AbstractAction {
    private final List<Filter> filters;

    public ResetFiltersAction(List<Filter> filters) {
        this.filters = filters;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ((TableRowSorter) DataAccess.getRecipeTable().getRowSorter()).setRowFilter(null);

        filters.forEach(Filter::resetFilter);
    }
}
