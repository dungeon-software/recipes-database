package cz.muni.fi.pv168.recipesdb.data.storage.repository;

import cz.muni.fi.pv168.recipesdb.data.storage.dao.CategoryDao;
import cz.muni.fi.pv168.recipesdb.data.storage.valueobject.CategoryValueObject;
import cz.muni.fi.pv168.recipesdb.data.storage.mapper.EntityMapper;
import cz.muni.fi.pv168.recipesdb.model.Category;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Vít Šebela
 */
public class CategoryRepository implements Repository<Category>{
    private final CategoryDao dao;
    private final EntityMapper<CategoryValueObject, Category> mapper;
    private List<Category> categories;

    public CategoryRepository(CategoryDao dao, EntityMapper<CategoryValueObject, Category> mapper) {
        this.dao = dao;
        this.mapper = mapper;
        this.categories = new ArrayList<>();
        refresh();
    }

    @Override
    public int getSize() {
        return categories.size();
    }

    @Override
    public Optional<Category> findByName(String name) {
        return categories.stream()
                .filter(category -> category.getName().equals(name))
                .findFirst();
    }

    @Override
    public Optional<Category> findByIndex(int index) {
        if (index < getSize()) {
            return Optional.of(categories.get(index));
        }
        return Optional.empty();
    }

    @Override
    public List<Category> findAll() {
        return List.copyOf(categories);
    }

    @Override
    public void refresh() {
        categories = dao.findAll().stream()
                .map(mapper::mapToModel)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    @Override
    public void create(Category newEntity) {
        dao.create(mapper.mapToDatabaseEntity(newEntity));
        refresh();
    }

    @Override
    public void update(Category entity) {
        dao.update(mapper.mapToDatabaseEntity(entity));
        refresh();
    }

    @Override
    public void deleteByName(String name) {
        dao.deleteByName(name);
        refresh();
    }
}
