package cz.muni.fi.pv168.recipesdb.ui.controller;

import cz.muni.fi.pv168.recipesdb.data.storage.repository.Repository;
import cz.muni.fi.pv168.recipesdb.model.Category;
import cz.muni.fi.pv168.recipesdb.model.Ingredient;
import cz.muni.fi.pv168.recipesdb.model.Recipe;
import cz.muni.fi.pv168.recipesdb.model.Unit;
import cz.muni.fi.pv168.recipesdb.model.WeightedIngredient;
import cz.muni.fi.pv168.recipesdb.ui.action.AddAction;
import cz.muni.fi.pv168.recipesdb.ui.action.DeleteAction;
import cz.muni.fi.pv168.recipesdb.ui.action.EditAction;
import cz.muni.fi.pv168.recipesdb.ui.dialog.CategoryDialog;
import cz.muni.fi.pv168.recipesdb.ui.dialog.EntityDialog;
import cz.muni.fi.pv168.recipesdb.ui.dialog.IngredientDialog;
import cz.muni.fi.pv168.recipesdb.ui.dialog.RecipeDialog;
import cz.muni.fi.pv168.recipesdb.ui.dialog.RecipeIngredientDialog;
import cz.muni.fi.pv168.recipesdb.ui.dialog.UnitDialog;
import cz.muni.fi.pv168.recipesdb.ui.model.Column;
import cz.muni.fi.pv168.recipesdb.ui.model.GenericListTableModel;
import cz.muni.fi.pv168.recipesdb.ui.model.GenericRepositoryTableModel;
import cz.muni.fi.pv168.recipesdb.ui.model.UnitRepositoryTableModel;
import cz.muni.fi.pv168.recipesdb.util.Formatter;

import java.util.function.Function;
import java.util.List;
import java.util.function.Supplier;
import javax.swing.JPopupMenu;
import javax.swing.JTable;

/**
 * Class used for creating tables using custom data models
 *
 * @author Vít Šebela
 * @author Stanislav Zeman
 */
public class TableCreator {
    /**
     * Creates new table with recipes.
     * When creating the table, we have to specify the columns that the table will hold.
     *
     * @param recipes List with recipes
     * @return Created JTable object with recipes
     */
    public static JTable createRecipeTable(Repository<Recipe> recipes) {
        List<Column<Recipe, ?>> columns = List.of(
                Column.readonly("Name", String.class, Recipe::getName),
                Column.readonly("Est. Time", String.class, Formatter::recipeEstimatedDurationMinutes),
                Column.readonly("Nutritional Value", String.class, Formatter::recipeNutritionalValueCalories),
                Column.readonly("Category", Category.class, Recipe::getCategory)
        );
        var model = new GenericRepositoryTableModel<>(recipes, columns);
        var table = new JTable(model);

        table.getColumn("Category").setCellRenderer(new RecipeColorRenderer());
        return setupTable(table,
                RecipeDialog::createEmptyRecipeDialog,
                RecipeDialog::createRecipeDialog);
    }

    /**
     * Creates new table with categories.
     * When creating the table, we have to specify the columns that the table will hold.
     *
     * @param categories List with categories
     * @return JTable object with categories
     */
    public static JTable createCategoryTable(Repository<Category> categories) {
        List<Column<Category, ?>> columns = List.of(
                Column.readonly("Category", String.class, Category::getName),
                Column.readonly("Color", String.class, category -> category.getColor().name())
        );
        var model = new GenericRepositoryTableModel<>(categories, columns);
        var table = new JTable(model);

        table.getColumn("Color").setCellRenderer(new CategoryColorRenderer());
        return setupTable(table,
                CategoryDialog::createEmptyCategoryDialog,
                CategoryDialog::createCategoryDialog);
    }

    /**
     * Creates new table with ingredients.
     * When creating the table, we have to specify the columns that the table will hold.
     *
     * @param ingredients List with ingredients
     * @return JTable object with ingredients
     */
    public static JTable createIngredientTable(Repository<Ingredient> ingredients) {
        List<Column<Ingredient, ?>> columns = List.of(
                Column.readonly("Ingredient", String.class, Ingredient::getName),
                Column.readonly("Units", String.class, Formatter::ingredientUnitWithAcronym),
                Column.readonly("Nutritional Value", String.class, Formatter::ingredientNutritionalValue));
        var model = new GenericRepositoryTableModel<>(ingredients, columns);
        var table = new JTable(model);

        return setupTable(table,
                IngredientDialog::createEmptyIngredientDialog,
                IngredientDialog::createIngredientDialog);
    }

    /**
     * Creates new table with units.
     * When creating the table, we have to specify the columns that the table will hold.
     *
     * @param units List with units
     * @return JTable table with units
     */
    public static JTable createUnitTable(Repository<Unit> units) {
        List<Column<Unit, ?>> columns = List.of(
                Column.readonly("Unit", String.class, Unit::getName),
                Column.readonly("Acronym", String.class, Unit::getAcronym),
                Column.readonly("Conversion rate", String.class, Formatter::unitConversion));
        var model = new UnitRepositoryTableModel(units, columns);
        var table = new JTable(model);

        return setupTable(table,
                UnitDialog::createEmptyUnitDialog,
                UnitDialog::createUnitDialog);
    }

    /**
     * Creates a new table filled with ingredients from the weighted ingredients.
     * When creating the table, we have to specify the columns that the table will hold.
     *
     * @param weightedIngredients List of weighted ingredients
     * @return JTable table with filled with ingredients and their amounts
     */
    public static JTable createRecipeIngredientDialogTable(List<WeightedIngredient> weightedIngredients) {
        List<Column<WeightedIngredient, ?>> columns = List.of(
                Column.readonly("Ingredient", String.class, Formatter::weightedIngredient),
                Column.readonly("Amount", String.class, Formatter::weightedIngredientAmount));
        var model = new GenericListTableModel<>(weightedIngredients, columns);
        var table = new JTable(model);

        return setupTable(table,
                RecipeIngredientDialog::createEmptyRecipeIngredientDialog,
                RecipeIngredientDialog::createRecipeIngredientDialog);
    }

    private static <T> JTable setupTable(JTable table,
                                         Supplier<EntityDialog<T>> createEmptyDialog,
                                         Function<T, EntityDialog<T>> createPrefilledDialog) {

        var popupMenu = new JPopupMenu();
        table.setAutoCreateRowSorter(true);
        table.getTableHeader().setReorderingAllowed(false);
        table.setComponentPopupMenu(popupMenu);

        popupMenu.add(new AddAction<>(table, createEmptyDialog));
        popupMenu.add(new EditAction<>(table, createPrefilledDialog));
        popupMenu.add(new DeleteAction(table));

        return table;
    }
}
