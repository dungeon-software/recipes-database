package cz.muni.fi.pv168.recipesdb.util;

import javax.swing.JOptionPane;

/**
 * @author Stanislav Zeman
 * @since 21.11.2022
 */
public class Notificator {

    public static void showErrorDialog(String error) {
        JOptionPane.showMessageDialog(null,
                error,
                "Something went wrong.", JOptionPane.ERROR_MESSAGE);
    }
    public static void showInvalidJsonFile(int lineNumber) {
        JOptionPane.showMessageDialog(null,
                """
                        JSON file is invalid!
                        Check line: %s.
                        """.formatted(lineNumber),
                "Invalid JSON file", JOptionPane.ERROR_MESSAGE);
    }


    public static void showUnparseableNumberNotification(String field, String numberType) {
        JOptionPane.showMessageDialog(null,
                field + " was not parseable!" + System.lineSeparator()
                        + "Please make sure you entered a valid " + numberType + " number!",
                "Error - wrong " + numberType + " number format", JOptionPane.ERROR_MESSAGE);
    }

    public static void showMissingCategoriesNotification() {
        JOptionPane.showMessageDialog(null,
                """
                        At least a single category has to exist before you can create a recipe!
                        """,
                "Error - Missing categories", JOptionPane.ERROR_MESSAGE);
    }

    public static void showMissingIngredientNotification() {
        JOptionPane.showMessageDialog(null,
                """
                           At least one ingredient has to exist before you can assign any to a recipe!
                        """,
                "Error - Missing ingredients", JOptionPane.ERROR_MESSAGE);
    }

    public static void showInvalidReferentialIntegrityDelete() {
        JOptionPane.showMessageDialog(null,
                """
                        Deleting an entity failed because other entity depends on it!
                        You need to delete the entity which references it first!
                        """,
                "Error - Dependency", JOptionPane.ERROR_MESSAGE);
    }

    public static void showObjectNameAlreadyExistsNotification() {
        JOptionPane.showMessageDialog(null,
                """
                        You entered a name which is already assigned to an object of the same type!
                        Either use another name or delete the objects associated with this name!
                        """,
                "Error - Name already used", JOptionPane.ERROR_MESSAGE);
    }

    public static void showStringLengthValidatorErrorNotification(String name, int min, int max) {
        JOptionPane.showMessageDialog(
                null,
                "'%s' is not between %d (inclusive) and %d (inclusive)"
                        .formatted(name, min, max),
                "Filter error", JOptionPane.ERROR_MESSAGE
        );
    }

    public static void showNumberRangeValidatorErrorNotification(String name, int lowerBound, int upperBound) {
        JOptionPane.showMessageDialog(
                null,
                "'%s' is not between %d (inclusive) and %d (exclusive)"
                        .formatted(name, lowerBound, upperBound),
                "Filter error", JOptionPane.ERROR_MESSAGE
        );
    }

    public static void showNumberLowerBoundValidatorErrorNotification(String name, int lowerBound) {
        JOptionPane.showMessageDialog(
                null,
                "'%s' can't be lower than %d"
                        .formatted(name, lowerBound),
                "Filter error", JOptionPane.ERROR_MESSAGE
        );
    }

}
