package cz.muni.fi.pv168.recipesdb.ui.dialog;

import java.util.Optional;
import javax.swing.JComponent;

/**
 * @author Vít Šebela
 */
public interface ShowableDialog {
    Optional<?> show(JComponent parentComponent, String title);
    Class getDialogClassType();
}
