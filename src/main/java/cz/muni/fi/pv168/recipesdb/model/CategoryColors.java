package cz.muni.fi.pv168.recipesdb.model;

/**
 * Enum of colors used in category color implementation
 * @author Vít Šebela
 */
public enum CategoryColors {
    WHITE,
    BLACK,
    GRAY,
    BLUE,
    LIGHTBLUE,
    DARKBLUE,
    ORANGE,
    MAGENTA,
    PINK,
    RED,
    LIGHTRED,
    DARKRED,
    YELLOW,
    LIGHTYELLOW,
    DARKYELLOW,
    GREEN,
    LIGHTGREEN,
    DARKGREEN,
    BROWN,
    PURPLE,
    CYAN,
    TURQUOISE,
    VIOLET,
    WHEAT,
    WHITESMOKE,
    LAVENDER,
    KHAKI,
    IVORY,
    GOLD
}
