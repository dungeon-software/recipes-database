package cz.muni.fi.pv168.recipesdb.ui.dialog;

import cz.muni.fi.pv168.recipesdb.data.DataAccess;
import cz.muni.fi.pv168.recipesdb.model.Ingredient;
import cz.muni.fi.pv168.recipesdb.model.WeightedIngredient;
import cz.muni.fi.pv168.recipesdb.ui.model.ComboBoxModelAdapter;
import cz.muni.fi.pv168.recipesdb.util.Notificator;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.xml.crypto.Data;
import java.util.stream.Stream;


public class RecipeIngredientDialog extends EntityDialog<WeightedIngredient> {
    private final ComboBoxModel<Ingredient> ingredients
            = new ComboBoxModelAdapter<>(DataAccess.getIngredientListModel());
    private final JTextField amount = new JTextField();
    private final WeightedIngredient weightedIngredient;

    private RecipeIngredientDialog(WeightedIngredient weightedIngredient) {
        this.weightedIngredient = weightedIngredient;
        setInitialValues();
        addFields();
    }

    public static <T> RecipeIngredientDialog createRecipeIngredientDialog(T weightedIngredient) {
        return new RecipeIngredientDialog((WeightedIngredient) weightedIngredient);
    }

    public static RecipeIngredientDialog createEmptyRecipeIngredientDialog() {
        if (DataAccess.getIngredientList().isEmpty()) {
            Notificator.showMissingIngredientNotification();
        }
        return new RecipeIngredientDialog(
                new WeightedIngredient(DataAccess.getIngredientList().get(0), 0.0));
    }

    private void setInitialValues() {
        ingredients.setSelectedItem(weightedIngredient.getIngredient());
        amount.setText(weightedIngredient.getAmount().toString());
    }

    private void addFields() {
        add("Ingredient:", new JComboBox<>(ingredients));
        add("Amount:", amount);
    }

    @Override
    WeightedIngredient getEntity() {
        weightedIngredient.setAmount(Double.parseDouble(amount.getText()));
        weightedIngredient.setIngredient((Ingredient) ingredients.getSelectedItem());
        return weightedIngredient;
    }
}
