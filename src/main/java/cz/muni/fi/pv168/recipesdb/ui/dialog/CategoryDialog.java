package cz.muni.fi.pv168.recipesdb.ui.dialog;

import cz.muni.fi.pv168.recipesdb.data.DataAccess;
import cz.muni.fi.pv168.recipesdb.model.Category;
import cz.muni.fi.pv168.recipesdb.model.CategoryColors;
import cz.muni.fi.pv168.recipesdb.ui.model.ComboBoxModelAdapter;

import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTextField;

public class CategoryDialog extends EntityDialog<Category> {
    private final JTextField name = new JTextField();
    private final ComboBoxModel<CategoryColors> colors = new ComboBoxModelAdapter<>(DataAccess.getCategoryColorsListModel());
    private final Category category;

    private CategoryDialog(Category category) {
        this.category = category;
        setInitialValues();
        addFields();
    }

    public static <T> CategoryDialog createCategoryDialog(T category) {
        return new CategoryDialog((Category) category);
    }

    public static CategoryDialog createEmptyCategoryDialog() {
        return new CategoryDialog(
                new Category("", CategoryColors.RED)
                );
    }

    private void setInitialValues() {
        name.setText(category.getName());
        colors.setSelectedItem(category.getColor());
    }

    private void addFields() {
        if (name.getText().equals("")) {
            add("Name:", name);
        }
        add("Color:", new JComboBox<>(colors));
    }

    @Override
    Category getEntity() {
        category.setName(name.getText());
        category.setColor((CategoryColors) colors.getSelectedItem());
        return category;
    }
}
