package cz.muni.fi.pv168.recipesdb.data.storage.dao;

import cz.muni.fi.pv168.recipesdb.data.storage.DataStorageException;
import cz.muni.fi.pv168.recipesdb.data.storage.database.ConnectionHandler;
import cz.muni.fi.pv168.recipesdb.data.storage.valueobject.CategoryValueObject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * @author Vít Šebela
 */
public class CategoryDao implements Dao<CategoryValueObject> {
    private final Supplier<ConnectionHandler> connections;

    public CategoryDao(Supplier<ConnectionHandler> connections) {
        this.connections = connections;
    }

    @Override
    public CategoryValueObject create(CategoryValueObject entity) {
        String query = """
                    INSERT INTO Category (name, color)
                    VALUES (?,?);
                    """;
        try (var conn = connections.get();
             var preparedStatement = conn.use().prepareStatement(query)) {
            preparedStatement.setString(1, entity.name());
            preparedStatement.setString(2, entity.color());

            preparedStatement.executeUpdate();

            return findByName(entity.name()).orElseThrow();

        } catch (SQLException e) {
            throw new DataStorageException("Failed to create new category", e);
        }
    }

    @Override
    public Collection<CategoryValueObject> findAll() {
        String query = """
                    SELECT name, color
                    FROM Category
                    """;
        try (var conn = connections.get();
             var preparedStatement =
                     conn.use().prepareStatement(query)) {

            List<CategoryValueObject> categories = new ArrayList<>();
            try (var resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    var category = getCategoryFromResultSet(resultSet);
                    categories.add(category);
                }
            }

            return List.copyOf(categories);

        } catch (SQLException e) {
            throw new DataStorageException("Could not read from database", e);
        }
    }

    @Override
    public Optional<CategoryValueObject> findByName(String name) {
        String query = """
                    SELECT name, color
                    FROM Category
                    WHERE name = ?;
                    """;
        try (var conn = connections.get();
             var preparedStatement =
                     conn.use().prepareStatement(query)) {

            preparedStatement.setString(1, name);
            try (var resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(getCategoryFromResultSet(resultSet));
                }
                return Optional.empty();
            }

        } catch (SQLException e) {
            throw new DataStorageException("Could not read from database", e);
        }
    }

    @Override
    public CategoryValueObject update(CategoryValueObject entity) {
        String query = """
                    UPDATE Category
                    SET (color) = (?)
                    WHERE name = ?;
                    """;
        try (var conn = connections.get();
             var preparedStatement =
                     conn.use().prepareStatement(query)) {

            preparedStatement.setString(1, entity.color());
            preparedStatement.setString(2, entity.name());
            preparedStatement.executeUpdate();

            return findByName(entity.name()).orElseThrow();

        } catch (SQLException e) {
            throw new DataStorageException("Could not read from database", e);
        }
    }

    @Override
    public void deleteByName(String name) {
        String query = """
                    DELETE FROM Category
                    WHERE name = ?
                    """;
        try (var conn = connections.get();
             var preparedStatement =
                     conn.use().prepareStatement(query)) {

            preparedStatement.setString(1, name);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DataStorageException("Could not delete category", e);
        }
    }

    private CategoryValueObject getCategoryFromResultSet(ResultSet resultSet) throws SQLException {
        return new CategoryValueObject(
                resultSet.getString("name"),
                resultSet.getString("color"));
    }
}
