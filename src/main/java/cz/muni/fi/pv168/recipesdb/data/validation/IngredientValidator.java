package cz.muni.fi.pv168.recipesdb.data.validation;

import cz.muni.fi.pv168.recipesdb.data.validation.common.NumberLowerBoundValidator;
import cz.muni.fi.pv168.recipesdb.data.validation.common.StringLengthValidator;
import cz.muni.fi.pv168.recipesdb.model.Ingredient;
import java.util.List;

import static cz.muni.fi.pv168.recipesdb.data.validation.Validator.extracting;

public class IngredientValidator implements Validator<Ingredient> {

    @Override
    public ValidationResult validate(Ingredient model) {

        var validators = List.of(
                extracting(Ingredient::getName,
                        new StringLengthValidator(1, 50, "Ingredient name")),
                extracting(Ingredient::getNutritionalValue,
                        new NumberLowerBoundValidator(0, "Nutritional value"))
        );

        return Validator.compose(validators).validate(model);
    }
}
