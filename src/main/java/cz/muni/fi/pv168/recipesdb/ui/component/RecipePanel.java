package cz.muni.fi.pv168.recipesdb.ui.component;

import cz.muni.fi.pv168.recipesdb.data.DataAccess;
import cz.muni.fi.pv168.recipesdb.model.Category;
import cz.muni.fi.pv168.recipesdb.model.Ingredient;
import cz.muni.fi.pv168.recipesdb.ui.action.ApplyFiltersAction;
import cz.muni.fi.pv168.recipesdb.ui.action.ResetFiltersAction;
import cz.muni.fi.pv168.recipesdb.ui.filter.ComboBoxFilter;
import cz.muni.fi.pv168.recipesdb.ui.filter.ComboBoxFilterType;
import cz.muni.fi.pv168.recipesdb.ui.filter.Filter;
import cz.muni.fi.pv168.recipesdb.ui.filter.RangeFilter;
import cz.muni.fi.pv168.recipesdb.ui.filter.RangeFilterType;
import java.awt.Font;
import java.util.List;
import java.util.function.Supplier;
import javax.swing.*;

import cz.muni.fi.pv168.recipesdb.wiring.DependencyProvider;
import net.miginfocom.swing.MigLayout;

public class RecipePanel extends JPanel {
    JTextField nutritionalValueFrom = new JTextField(10);
    JTextField nutritionalValueTo = new JTextField(10);
    JTextField portionFrom = new JTextField(10);
    JTextField portionTo = new JTextField(10);
    JTextField estimatedTimeFrom = new JTextField(10);
    JTextField estimatedTimeTo = new JTextField(10);
    JComboBox<Object> ingredientComboBox;
    JComboBox<Object> categoryComboBox;

    public RecipePanel(JTable recipeTable, DependencyProvider dependencyProvider) {
        super(false);
        setLayout(new MigLayout("", "grow", "grow, fill"));

        ingredientComboBox = new RefreshableComboBox<>(dependencyProvider.getIngredientRepository());
        categoryComboBox = new RefreshableComboBox<>(dependencyProvider.getCategoryRepository());

        addRecipeFromToFilter("Nutritional Value", nutritionalValueFrom, nutritionalValueTo);
        addRecipeFromToFilter("Recipe Time", estimatedTimeFrom, estimatedTimeTo);
        addRecipeFromToFilter("Portion Size", portionFrom, portionTo);

        addRecipeComboBoxFilter("Categories", "split 2, gapright 20px", categoryComboBox);
        addRecipeComboBoxFilter("Ingredients", "split 2", ingredientComboBox);

        add(new JSeparator(), " wrap");

        addFilterActionButtons();

        add(new JScrollPane(recipeTable), "grow, wrap");
    }

    private void addRecipeComboBoxFilter(String filterName, String constraints, JComboBox<Object> comboBox) {
        add(createFilterHeader(filterName), constraints);
        add(comboBox, "wrap");
    }

    private void addFilterActionButtons() {
        List<Filter> filters = List.of(
                new RangeFilter(RangeFilterType.RECIPE_NUTRITIONAL_VALUE, nutritionalValueFrom, nutritionalValueTo),
                new RangeFilter(RangeFilterType.RECIPE_ESTIMATED_TIME, estimatedTimeFrom, estimatedTimeTo),
                new RangeFilter(RangeFilterType.RECIPE_PORTION_SIZE, portionFrom, portionTo),
                new ComboBoxFilter(ComboBoxFilterType.RECIPE_CATEGORY, categoryComboBox),
                new ComboBoxFilter(ComboBoxFilterType.RECIPE_INGREDIENT, ingredientComboBox)
        );

        JButton applyFilterButton = new JButton("Apply filters");
        applyFilterButton.addActionListener(new ApplyFiltersAction(filters));
        add(applyFilterButton, "split 3");

        add(new JSeparator(SwingConstants.VERTICAL));

        JButton resetFilterButton = new JButton("Reset filters");
        resetFilterButton.addActionListener(new ResetFiltersAction(filters));
        add(resetFilterButton,"wrap");
    }

    private void addRecipeFromToFilter(String filterName, JTextField fromField, JTextField toField) {
        add(createFilterHeader(filterName), "wrap");
        add(new JLabel("From"), "split 4");
        add(fromField);
        add(new JLabel("To"));
        add(toField, "wrap");
        add(new JSeparator(), " wrap");
    }

    private JLabel createFilterHeader(String filterName) {
        JLabel filterHeader = new JLabel(filterName);
        filterHeader.setFont((new Font("Arial", Font.BOLD, 11)));
        return filterHeader;
    }
}
