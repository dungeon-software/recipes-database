package cz.muni.fi.pv168.recipesdb.ui.filter.rowfilter;

import cz.muni.fi.pv168.recipesdb.model.Category;
import cz.muni.fi.pv168.recipesdb.model.Recipe;
import cz.muni.fi.pv168.recipesdb.ui.model.GenericTableModel;
import javax.swing.JTable;
import javax.swing.RowFilter;

public class CategoryRowFilter extends RowFilter<Object, Object> {
    private final Category category;
    private final JTable table;

    public CategoryRowFilter(JTable table, Category category) {
        this.table = table;
        this.category = category;
    }

    @Override
    public boolean include(Entry entry) {
        GenericTableModel<Recipe> tableModel = (GenericTableModel) table.getModel();
        Recipe recipe = tableModel.getEntity((Integer) entry.getIdentifier());

        return recipe.getCategory().equals(category);
    }
}
