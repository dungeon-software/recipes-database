package cz.muni.fi.pv168.recipesdb.ui.action;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.AbstractAction;

public final class QuitAction extends AbstractAction {

    public QuitAction() {
        super("Quit");
        putValue(SHORT_DESCRIPTION, "Quit RecipesDB");
        putValue(MNEMONIC_KEY, KeyEvent.VK_Q);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.exit(0);
    }
}
