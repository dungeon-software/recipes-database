package cz.muni.fi.pv168.recipesdb.data.validation;

import cz.muni.fi.pv168.recipesdb.data.validation.common.StringLengthValidator;
import cz.muni.fi.pv168.recipesdb.model.Category;
import java.util.List;

import static cz.muni.fi.pv168.recipesdb.data.validation.Validator.extracting;

public class CategoryValidator implements Validator<Category> {

    @Override
    public ValidationResult validate(Category model) {

        var validators = List.of(
                extracting(Category::getName,
                        new StringLengthValidator(1, 50, "Category name"))
        );

        return Validator.compose(validators).validate(model);
    }
}