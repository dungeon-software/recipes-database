package cz.muni.fi.pv168.recipesdb.data.validation;

import cz.muni.fi.pv168.recipesdb.data.validation.common.NumberLowerBoundValidator;
import cz.muni.fi.pv168.recipesdb.data.validation.common.StringLengthValidator;
import cz.muni.fi.pv168.recipesdb.model.Unit;
import java.util.List;

public class UnitValidator implements Validator<Unit> {

    @Override
    public ValidationResult validate(Unit model) {

        var validators = List.of(
                Validator.extracting(Unit::getName,
                        new StringLengthValidator(1, 50, "Unit name")),
                Validator.extracting(Unit::getAcronym,
                        new StringLengthValidator(1, 10, "Acronym")),
                Validator.extracting(Unit::getIntegerConversionRatio,
                        new NumberLowerBoundValidator(0, "Conversion ratio"))
        );

        return Validator.compose(validators).validate(model);
    }
}
