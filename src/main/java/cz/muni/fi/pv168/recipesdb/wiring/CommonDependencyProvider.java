package cz.muni.fi.pv168.recipesdb.wiring;

import cz.muni.fi.pv168.recipesdb.data.manipulation.Exporter;
import cz.muni.fi.pv168.recipesdb.data.manipulation.Importer;
import cz.muni.fi.pv168.recipesdb.data.manipulation.RecipeJsonExporter;
import cz.muni.fi.pv168.recipesdb.data.manipulation.RecipeJsonImporter;
import cz.muni.fi.pv168.recipesdb.data.storage.dao.CategoryDao;
import cz.muni.fi.pv168.recipesdb.data.storage.dao.IngredientDao;
import cz.muni.fi.pv168.recipesdb.data.storage.dao.RecipeDao;
import cz.muni.fi.pv168.recipesdb.data.storage.dao.UnitDao;
import cz.muni.fi.pv168.recipesdb.data.storage.database.DatabaseManager;
import cz.muni.fi.pv168.recipesdb.data.storage.mapper.CategoryMapper;
import cz.muni.fi.pv168.recipesdb.data.storage.mapper.IngredientMapper;
import cz.muni.fi.pv168.recipesdb.data.storage.mapper.RecipeMapper;
import cz.muni.fi.pv168.recipesdb.data.storage.mapper.UnitMapper;
import cz.muni.fi.pv168.recipesdb.data.storage.repository.CategoryRepository;
import cz.muni.fi.pv168.recipesdb.data.storage.repository.IngredientRepository;
import cz.muni.fi.pv168.recipesdb.data.storage.repository.RecipeRepository;
import cz.muni.fi.pv168.recipesdb.data.storage.repository.Repository;
import cz.muni.fi.pv168.recipesdb.data.storage.repository.UnitRepository;
import cz.muni.fi.pv168.recipesdb.data.validation.CategoryValidator;
import cz.muni.fi.pv168.recipesdb.data.validation.IngredientValidator;
import cz.muni.fi.pv168.recipesdb.data.validation.RecipeValidator;
import cz.muni.fi.pv168.recipesdb.data.validation.UnitValidator;
import cz.muni.fi.pv168.recipesdb.data.validation.Validator;
import cz.muni.fi.pv168.recipesdb.model.Category;
import cz.muni.fi.pv168.recipesdb.model.Ingredient;
import cz.muni.fi.pv168.recipesdb.model.Recipe;
import cz.muni.fi.pv168.recipesdb.model.Unit;

public abstract class CommonDependencyProvider implements DependencyProvider {
    private final DatabaseManager databaseManager;
    private final Repository<Category> categories;
    private final Repository<Unit> units;
    private final Repository<Recipe> recipes;
    private final Repository<Ingredient> ingredients;
    private final Validator<Recipe> recipeValidator;
    private final Validator<Category> categoryValidator;
    private final Validator<Ingredient> ingredientValidator;
    private final Validator<Unit> unitValidator;
    private Importer importer;
    private Exporter exporter;

    protected CommonDependencyProvider(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
        this.recipeValidator = new RecipeValidator();
        this.categoryValidator = new CategoryValidator();
        this.ingredientValidator = new IngredientValidator();
        this.unitValidator = new UnitValidator();

        this.categories = new CategoryRepository(
                new CategoryDao(databaseManager::getConnectionHandler),
                new CategoryMapper(categoryValidator)
        );

        this.units = new UnitRepository(
                new UnitDao(databaseManager::getConnectionHandler),
                new UnitMapper(unitValidator)
        );

        this.ingredients = new IngredientRepository(
                new IngredientDao(databaseManager::getConnectionHandler),
                new IngredientMapper(units, ingredientValidator)
        );

        this.recipes = new RecipeRepository(
                new RecipeDao(databaseManager::getConnectionHandler),
                new RecipeMapper(ingredients, categories, recipeValidator)
        );
    }

    @Override
    public DatabaseManager getDatabaseManager() {
        return databaseManager;
    }

    @Override
    public Repository<Recipe> getRecipeRepository() {
        return recipes;
    }

    @Override
    public Repository<Unit> getUnitRepository() {
        return units;
    }

    @Override
    public Repository<Category> getCategoryRepository() {
        return categories;
    }

    @Override
    public Repository<Ingredient> getIngredientRepository() {
        return ingredients;
    }

    @Override
    public Importer getImporter() {
        if (importer == null) {
            importer = new RecipeJsonImporter(recipes);
        }
        return importer;
    }

    @Override
    public Exporter getExporter() {
        if (exporter == null) {
            exporter = new RecipeJsonExporter();
        }
        return exporter;
    }

    @Override
    public Validator<Recipe> getRecipeValidator() {return recipeValidator;}

    @Override
    public Validator<Unit> getUnitValidator() {return unitValidator;}

    @Override
    public Validator<Ingredient> getIngredientValidator() {return ingredientValidator;}

    @Override
    public Validator<Category> getCategoryValidator() {return categoryValidator;}

}
