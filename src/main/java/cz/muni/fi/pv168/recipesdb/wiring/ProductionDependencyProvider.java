package cz.muni.fi.pv168.recipesdb.wiring;

import cz.muni.fi.pv168.recipesdb.data.storage.database.DatabaseManager;

public class ProductionDependencyProvider extends CommonDependencyProvider {

    public ProductionDependencyProvider() {
        super(createDatabaseManager());
    }

    private static DatabaseManager createDatabaseManager() {
        DatabaseManager databaseManager = DatabaseManager.createProductionInstance();
        databaseManager.initSchema();

        return databaseManager;
    }
}
