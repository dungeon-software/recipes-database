package cz.muni.fi.pv168.recipesdb.ui.model;

import java.util.List;
import javax.swing.AbstractListModel;

public class GenericListModel<E> extends AbstractListModel<E> {
    private final List<E> items;

    public GenericListModel(List<E> items) {
        this.items = items;
    }

    @Override
    public int getSize() {
        return items.size();
    }

    @Override
    public E getElementAt(int index) {
        return items.get(index);
    }
}

