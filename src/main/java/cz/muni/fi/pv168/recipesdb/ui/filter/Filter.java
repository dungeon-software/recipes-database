package cz.muni.fi.pv168.recipesdb.ui.filter;

import javax.swing.RowFilter;

public interface Filter {
    RowFilter<Object, Object> getRowFilter();
    void resetFilter();
    boolean contentIsValid();
}
