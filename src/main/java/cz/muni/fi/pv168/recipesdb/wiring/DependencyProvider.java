package cz.muni.fi.pv168.recipesdb.wiring;

import cz.muni.fi.pv168.recipesdb.data.manipulation.Exporter;
import cz.muni.fi.pv168.recipesdb.data.manipulation.Importer;
import cz.muni.fi.pv168.recipesdb.data.storage.database.DatabaseManager;
import cz.muni.fi.pv168.recipesdb.data.storage.repository.Repository;
import cz.muni.fi.pv168.recipesdb.data.validation.Validator;
import cz.muni.fi.pv168.recipesdb.model.Category;
import cz.muni.fi.pv168.recipesdb.model.Ingredient;
import cz.muni.fi.pv168.recipesdb.model.Recipe;
import cz.muni.fi.pv168.recipesdb.model.Unit;

public interface DependencyProvider {
    DatabaseManager getDatabaseManager();

    Repository<Recipe> getRecipeRepository();

    Repository<Unit> getUnitRepository();

    Repository<Category> getCategoryRepository();

    Repository<Ingredient> getIngredientRepository();

    Validator<Recipe> getRecipeValidator();

    Validator<Unit> getUnitValidator();
    Validator<Ingredient> getIngredientValidator();

    Validator<Category> getCategoryValidator();

    Importer getImporter();

    Exporter getExporter();
}
