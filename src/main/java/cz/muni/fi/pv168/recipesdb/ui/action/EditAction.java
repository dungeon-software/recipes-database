package cz.muni.fi.pv168.recipesdb.ui.action;

import cz.muni.fi.pv168.recipesdb.ui.dialog.EntityDialog;
import cz.muni.fi.pv168.recipesdb.ui.model.GenericTableModel;

import java.util.function.Function;
import javax.swing.AbstractAction;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.event.ListSelectionEvent;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

/**
 * Class representing edit action on a given UI table
 *
 * @author Vít Šebela
 * @author Stanislav Zeman
 * @param <T> Type of records stored in given the table
 */
public class EditAction<T> extends AbstractAction {

    private final JTable table;
    private final Function<T, EntityDialog<T>> createDialog;

    public EditAction(JTable table,
                      Function<T, EntityDialog<T>> createDialog) {
        super("Edit");
        this.table = table;
        this.createDialog = createDialog;
        this.table.getSelectionModel().addListSelectionListener(this::rowSelectionChanged);

        putValue(SHORT_DESCRIPTION, "Edits selected record");
        putValue(MNEMONIC_KEY, KeyEvent.VK_E);
        putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl E"));
    }

    private void rowSelectionChanged(ListSelectionEvent listSelectionEvent) {
        if (table.getSelectedRowCount() != 1) {
            this.setEnabled(false);
            return;
        }
        this.setEnabled(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int[] selectedRows = table.getSelectedRows();
        if (selectedRows.length != 1) {
            throw new IllegalStateException("Invalid selected rows count (must be 1): " + selectedRows.length);
        }
        if (table.isEditing()) {
            table.getCellEditor().cancelCellEditing();
        }
        var tableModel = (GenericTableModel) table.getModel();
        int modelRow = table.convertRowIndexToModel(selectedRows[0]);

        var record = (T) tableModel.getEntity(modelRow);
        var dialog = createDialog.apply(record);

        // TODO: Editing weightedIngredients on recipe has no effect
        dialog.show(table, "Edit Record")
                .ifPresent(tableModel::updateRow);
    }
}
