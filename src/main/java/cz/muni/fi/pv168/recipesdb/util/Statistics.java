package cz.muni.fi.pv168.recipesdb.util;

import cz.muni.fi.pv168.recipesdb.data.DataAccess;
import cz.muni.fi.pv168.recipesdb.data.storage.repository.Repository;
import cz.muni.fi.pv168.recipesdb.model.Category;
import cz.muni.fi.pv168.recipesdb.model.Ingredient;
import cz.muni.fi.pv168.recipesdb.model.Recipe;
import cz.muni.fi.pv168.recipesdb.model.Unit;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

/**
 * @author Stanislav Zeman
 * @since 12.10.2022
 */
public class Statistics {
    private static final Repository<Recipe> recipeRepository = DataAccess.getDependencyProvider().getRecipeRepository();
    private static  final Repository<Category> categorieRepository = DataAccess.getDependencyProvider().getCategoryRepository();
    private static final Repository<Ingredient> ingredientRepository = DataAccess.getDependencyProvider().getIngredientRepository();
    private static final Repository<Unit> unitRepository = DataAccess.getDependencyProvider().getUnitRepository();

    public static String generateStatistics() {
        StringBuilder sb = new StringBuilder("Statistics:")
                .append(System.lineSeparator()).append(System.lineSeparator());

        generateCounts(sb);
        generateCategoryStatistics(sb);
        generateAverages(sb);

        return sb.toString();
    }

    private static void generateCounts(StringBuilder sb) {
        sb.append("    Counts:").append(System.lineSeparator());
        sb.append("        Recipes: ").append(recipeRepository.getSize()).append(System.lineSeparator());
        sb.append("        Categories: ").append(categorieRepository.getSize()).append(System.lineSeparator());
        sb.append("        Ingredient: ").append(ingredientRepository.getSize()).append(System.lineSeparator());
        sb.append("        Units:").append(System.lineSeparator());
        sb.append("            Base units: ").append(unitRepository.findAll().stream()
                .filter(Unit::isBaseUnit)
                .count()).append(System.lineSeparator());
        sb.append("            Custom units: ").append(unitRepository.findAll().stream()
                .filter(Unit::isNotBaseUnit)
                .count()).append(System.lineSeparator());
        sb.append(System.lineSeparator());
    }

    private static void generateCategoryStatistics(StringBuilder sb) {
        generateCategoryCounts(sb);
        generateCategoryNutritionalAverages(sb);
    }

    private static void generateCategoryCounts(StringBuilder sb) {
        Map<Category, Long> categoryRecipesCount = recipeRepository.findAll().stream()
                .collect(
                        groupingBy(
                                Recipe::getCategory,
                                counting()
                        )
                );

        sb.append("    Number of recipes in categories:").append(System.lineSeparator());

        for (Map.Entry<Category, Long> entry : categoryRecipesCount.entrySet()) {
            sb.append("        ").append(entry.getKey()).append(": ")
                    .append(entry.getValue()).append(System.lineSeparator());
        }
        sb.append(System.lineSeparator());
    }

    private static void generateCategoryNutritionalAverages(StringBuilder sb) {
        Map<Category, List<Recipe>> categoryRecipes = recipeRepository.findAll().stream()
                .collect(
                        groupingBy(
                                Recipe::getCategory,
                                toList()
                        )
                );

        sb.append("    Average nutritional value in given categories:").append(System.lineSeparator());

        for (Map.Entry<Category, List<Recipe>> entry : categoryRecipes.entrySet()) {
            Integer nutritionalValue = generateAverageRecipeStatistic(entry.getValue(), Recipe::getNutritionalValue);
            sb.append("        ").append(entry.getKey()).append(": ")
                    .append(nutritionalValue).append(" kcal").append(System.lineSeparator());
        }
        sb.append(System.lineSeparator());
    }

    private static void generateAverages(StringBuilder sb) {
        sb.append(generateAverage("Average recipe preparation time", Recipe::getEstimatedDuration, "min"));
        sb.append(generateAverage("Average recipe calories", Recipe::getNutritionalValue, "kcal"));
        sb.append(generateAverage("Average recipe portions", Recipe::getPortions, "portions"));
    }

    private static String generateAverage(String header, Function<Recipe, Integer> function, String postfix) {
        return "    " + header + ": " + generateAverageRecipeStatistic(recipeRepository.findAll(), function) + " " + postfix + System.lineSeparator();
    }

    private static Integer generateAverageRecipeStatistic(List<Recipe> data, Function<Recipe, Integer> function) {
        return data
                .stream()
                .map(function)
                .reduce(0, Integer::sum) / data.size();
    }
}
