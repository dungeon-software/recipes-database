package cz.muni.fi.pv168.recipesdb.data.storage.repository;

import java.util.List;
import java.util.Optional;

/**
 * Interface for repositories which hold instances of data models
 * @author Vít Šebela
 * @param <M> Type of data model
 */
public interface Repository <M>{
    /**
     * Gets the number of objects in the repository
     * @return size of repository
     */
    int getSize();

    /**
     * Searches for an object with given name and returns it, if present
     * @param name which will be searched for
     * @return Empty if object with such name doesn't exist, Some object otherwise
     */
    Optional<M> findByName(String name);

    /**
     * Searches for an object with given index and returns it, if present
     * @param index of the object
     * @return Empty if object with such index doesn't exist, Some object otherwise
     */
    Optional<M> findByIndex(int index);

    /**
     * Returns all elements in the repository
     * @return all elements in repository
     */
    List<M> findAll();

    /**
     * Refreshes repository's content by querying data access object.
     * If repository data is outdated, loads new data into repository and synchronizes with the DAO.
     * This method needs to be called everytime when the database content changes
     */
    void refresh();

    /**
     * Inserts a new entity into database through the relevant DAO and refreshes itself
     * @param newEntity entity which will be created
     */
    void create(M newEntity);

    /**
     * Updates an already existing entity in database through the relevant DAO and refreshes itself
     * @param entity entity which will be refreshed
     */
    void update(M entity);

    /**
     * Deletes an entity in database through the relevant DAO and refreshes itself
     */
    void deleteByName(String name);
}
