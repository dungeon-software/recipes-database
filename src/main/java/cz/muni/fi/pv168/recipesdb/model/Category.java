package cz.muni.fi.pv168.recipesdb.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

/**
 * Class representing a food category
 *
 * @author Stanislav Zeman
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Category implements TableEntity {
    @NonNull
    private String name;
    @NonNull
    private CategoryColors color;

    @Override
    public String toString() {
        return name;
    }
}
