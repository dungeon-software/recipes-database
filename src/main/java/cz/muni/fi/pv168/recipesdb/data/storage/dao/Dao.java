package cz.muni.fi.pv168.recipesdb.data.storage.dao;

import cz.muni.fi.pv168.recipesdb.data.storage.DataStorageException;

import java.util.Collection;
import java.util.Optional;

/**
 * Interface for CRUD operations.
 *
 * @param <E> type of the entity this DAO operates on
 */
public interface Dao<E> {

    /**
     * Creates a new entity using the underlying data source.
     *
     * @param entity entity to be persisted
     * @return Entity instance with set id
     * @throws IllegalArgumentException when the entity has already been persisted
     * @throws DataStorageException     when anything goes wrong with the underlying data source
     */
    E create(E entity);

    /**
     * Reads all entities from the underlying data source.
     *
     * @return collection of all entities known to the underlying data source
     * @throws DataStorageException when anything goes wrong with the underlying data source
     */
    Collection<E> findAll();

    /**
     * Finds entity by name.
     *
     * @param name entity name
     * @return either empty if not found or the entity instance
     */
    Optional<E> findByName(String name);

    /**
     * Updates an entity using the underlying data source.
     *
     * @param entity entity to be deleted
     * @throws IllegalArgumentException when the entity has not been persisted yet
     * @throws DataStorageException     when anything goes wrong with the underlying data source
     */
    E update(E entity);

    /**
     * Deletes an entity using the underlying data source.
     *
     * @param name entity name to be deleted
     * @throws IllegalArgumentException when the entity has not been persisted yet
     * @throws DataStorageException     when anything goes wrong with the underlying data source
     */
    void deleteByName(String name);
}
