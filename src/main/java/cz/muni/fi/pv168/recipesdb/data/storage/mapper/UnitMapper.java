package cz.muni.fi.pv168.recipesdb.data.storage.mapper;

import cz.muni.fi.pv168.recipesdb.data.DataAccess;
import cz.muni.fi.pv168.recipesdb.data.storage.valueobject.UnitValueObject;
import cz.muni.fi.pv168.recipesdb.data.validation.Validator;
import cz.muni.fi.pv168.recipesdb.model.Unit;
import cz.muni.fi.pv168.recipesdb.model.UnitConversion;

/**
 * @author Vít Šebela
 */
public class UnitMapper implements EntityMapper<UnitValueObject, Unit> {
    private final Validator<Unit> unitValidator;

    public UnitMapper(Validator<Unit> unitValidator) {
        this.unitValidator = unitValidator;
    }

    @Override
    public UnitValueObject mapToDatabaseEntity(Unit model) {
        unitValidator.validate(model).intoException();

        return new UnitValueObject(
                model.getName(),
                model.getAcronym(),
                model.getUnitConversion().getAmount(),
                model.getUnitConversion().getUnit().getAcronym()
        );
    }

    @Override
    public Unit mapToModel(UnitValueObject entity) {
        return new Unit(
                entity.name(),
                entity.acronym(),
                new UnitConversion(
                        entity.amount(),
                        DataAccess.getBaseUnit(entity.baseUnit().toUpperCase())
                )
        );
    }
}
