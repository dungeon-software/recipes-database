package cz.muni.fi.pv168.recipesdb.data.storage.mapper;

import cz.muni.fi.pv168.recipesdb.data.storage.valueobject.CategoryValueObject;
import cz.muni.fi.pv168.recipesdb.data.validation.Validator;
import cz.muni.fi.pv168.recipesdb.model.Category;
import cz.muni.fi.pv168.recipesdb.model.CategoryColors;

/**
 * @author Vít Šebela
 */
public class CategoryMapper implements EntityMapper<CategoryValueObject, Category>{
    Validator<Category> categoryValidator;

    public CategoryMapper(Validator<Category> categoryValidator) {
        this.categoryValidator = categoryValidator;
    }

    @Override
    public CategoryValueObject mapToDatabaseEntity(Category model) {
        categoryValidator.validate(model).intoException();

        return new CategoryValueObject(
                model.getName(),
                model.getColor().name()
        );
    }

    @Override
    public Category mapToModel(CategoryValueObject entity) {
        return new Category(
                entity.name(),
                CategoryColors.valueOf(entity.color())
        );
    }
}
