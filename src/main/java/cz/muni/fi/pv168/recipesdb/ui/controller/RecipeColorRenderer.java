package cz.muni.fi.pv168.recipesdb.ui.controller;

import cz.muni.fi.pv168.recipesdb.model.Category;
import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import org.beryx.awt.color.ColorFactory;

/**
 * @author Vít Šebela
 */
public class RecipeColorRenderer extends DefaultTableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table,
                                                   Object value,
                                                   boolean isSelected,
                                                   boolean hasFocus,
                                                   int row,
                                                   int column) {

        Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        if (column == 3) {
            try {
                cell.setForeground(ColorFactory.valueOf(((Category) value).getColor().name()));
            } catch (IllegalArgumentException e) {
                cell.setForeground(Color.black);
            }
        } else {
            cell.setForeground(Color.black);
        }
        return cell;

    }
}
