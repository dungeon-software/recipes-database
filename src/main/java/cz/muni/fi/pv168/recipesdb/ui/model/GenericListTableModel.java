package cz.muni.fi.pv168.recipesdb.ui.model;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Vít Šebela
 */
public class GenericListTableModel<T> extends AbstractTableModel implements GenericTableModel<T> {
    private final List<T> records;
    private final List<Column<T,?>> columns;

    public GenericListTableModel(List<T> records, List<Column<T,?>> columns) {
        this.records = new ArrayList<>(records);
        this.columns = columns;
    }

    @Override
    public int getRowCount() {
        return records.size();
    }

    @Override
    public int getColumnCount() {
        return columns.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        T record = getEntity(rowIndex);
        return columns.get(columnIndex).getValue(record);
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columns.get(columnIndex).getName();
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return columns.get(columnIndex).getColumnType();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columns.get(columnIndex).isEditable();
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        var record = getEntity(rowIndex);
        columns.get(columnIndex).setValue(value, record);
    }

    public void deleteRow(int rowIndex) {
        records.remove(rowIndex);
        fireTableRowsDeleted(rowIndex, rowIndex);
    }

    public void addRow(T record) {
        int newRowIndex = records.size();
        records.add(record);
        fireTableRowsInserted(newRowIndex, newRowIndex);
    }

    public void updateRow(T record) {
        int rowIndex = records.indexOf(record);
        fireTableRowsUpdated(rowIndex, rowIndex);
    }

    public List<T> getRecords() {
        return Collections.unmodifiableList(records);
    }

    public T getEntity(int rowIndex) {
        return records.get(rowIndex);
    }
}
