package cz.muni.fi.pv168.recipesdb.data.manipulation;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import cz.muni.fi.pv168.recipesdb.model.Recipe;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;

/**
 * @author Vít Šebela
 */
public class RecipeJsonExporter implements Exporter{

    private final Gson gson;

    public RecipeJsonExporter() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        gsonBuilder.setLenient();
        gsonBuilder.serializeNulls();
        this.gson = gsonBuilder.create();
    }

    @Override
    public void exportRecipes(Collection<Recipe> recipes, String filePath) {
        try (PrintWriter printWriter = new PrintWriter(new FileWriter(filePath))) {
            System.out.println(recipes);
            printWriter.print(gson.toJson(recipes));
        } catch (IOException e) {
            throw new DataManipulationException("Cannot open or create file to write!",e);
        }
    }
}
