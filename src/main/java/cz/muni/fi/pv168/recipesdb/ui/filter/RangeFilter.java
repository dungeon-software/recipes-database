package cz.muni.fi.pv168.recipesdb.ui.filter;

import cz.muni.fi.pv168.recipesdb.ui.filter.rowfilter.RangeRowFilter;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.RowFilter;

public class RangeFilter implements Filter {
    private final JTextField fromTextField;
    private final JTextField toTextField;
    private final RangeFilterType filterType;

    public RangeFilter(RangeFilterType filterType, JTextField fromTextField, JTextField toTextField) {
        this.filterType = filterType;
        this.fromTextField = fromTextField;
        this.toTextField = toTextField;
    }

    public Integer getFrom() {
        String text = fromTextField.getText().trim();

        if (text.equals("")) {
            return Integer.MIN_VALUE;
        }
        return Integer.parseInt(text);
    }

    public Integer getTo() {
        String text = toTextField.getText().trim();

        if (text.equals("")) {
            return Integer.MAX_VALUE;
        }
        return Integer.parseInt(text);
    }

    @Override
    public RowFilter<Object, Object> getRowFilter() {
        return new RangeRowFilter(filterType, getFrom(), getTo());
    }

    @Override
    public void resetFilter() {
        fromTextField.setText("");
        toTextField.setText("");
    }

    private void showErrorMessage() {
        JOptionPane.showMessageDialog(
                null,
                "Only numbers can be used as input. " + System.lineSeparator() +
                        "Following filter contains incorrect input: " + System.lineSeparator() +
                        filterType.name().toLowerCase().replace('_', ' '),
                "Filter error", JOptionPane.ERROR_MESSAGE
        );
    }

    private boolean textFieldContentIsValid(JTextField textField) {
        String text = textField.getText().trim();

        if (text.equals("")) {
            return true;
        }

        try {
            return Integer.parseInt(text) >= 0;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    @Override
    public boolean contentIsValid() {
        boolean isValid = textFieldContentIsValid(fromTextField) && textFieldContentIsValid(toTextField);

        if (!isValid) {
            showErrorMessage();
        }

        return isValid;
    }
}
