package cz.muni.fi.pv168.recipesdb.ui.model;

import cz.muni.fi.pv168.recipesdb.data.storage.repository.Repository;
import cz.muni.fi.pv168.recipesdb.model.TableEntity;
import java.util.List;
import java.util.Optional;
import javax.swing.table.AbstractTableModel;

/**
 * @author Vít Šebela
 */
public class GenericRepositoryTableModel<T> extends AbstractTableModel implements GenericTableModel<T> {
    private final Repository<T> repository;
    private final List<Column<T,?>> columns;

    public GenericRepositoryTableModel(Repository<T> repository, List<Column<T,?>> columns) {
        this.repository = repository;
        this.columns = columns;
    }

    @Override
    public int getRowCount() {
        return repository.findAll().size();
    }

    @Override
    public int getColumnCount() {
        return columns.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        T record = getEntity(rowIndex);
        return columns.get(columnIndex).getValue(record);
    }

    @Override
    public String getColumnName(int columnIndex) {
        return columns.get(columnIndex).getName();
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return columns.get(columnIndex).getColumnType();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columns.get(columnIndex).isEditable();
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        var record = getEntity(rowIndex);
        columns.get(columnIndex).setValue(value, record);
    }

    public void deleteRow(int rowIndex) {
        Optional<T> entity = repository.findByIndex(rowIndex);
        if (entity.isEmpty()) {
            throw new ArrayIndexOutOfBoundsException("Row record with index: " + rowIndex + " doesn't exist");
        }
        TableEntity unwrapped = (TableEntity) entity.get();
        repository.deleteByName(unwrapped.getName());
        fireTableRowsDeleted(rowIndex, rowIndex);
    }

    public void addRow(T entity) {
        int index = repository.getSize();
        repository.create(entity);
        fireTableRowsInserted(index, index);
    }

    public void updateRow(T record) {
        repository.update(record);
        fireTableRowsUpdated(0, repository.getSize() - 1);
    }

    public List<T> getRecords() {
        return repository.findAll();
    }

    public Repository<T> getRepository() {
        return repository;
    }

    public T getEntity(int rowIndex) {
        Optional<T> entity = repository.findByIndex(rowIndex);
        if (entity.isEmpty()) {
            throw new ArrayIndexOutOfBoundsException("Row record with index: " + rowIndex + " doesn't exist");
        }
        return entity.get();
    }
}
