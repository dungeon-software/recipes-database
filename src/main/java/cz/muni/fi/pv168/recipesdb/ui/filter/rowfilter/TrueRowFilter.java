package cz.muni.fi.pv168.recipesdb.ui.filter.rowfilter;

import javax.swing.RowFilter;

public class TrueRowFilter extends RowFilter<Object, Object> {
    public TrueRowFilter() {
    }

    @Override
    public boolean include(Entry entry) {
        return true;
    }
}
