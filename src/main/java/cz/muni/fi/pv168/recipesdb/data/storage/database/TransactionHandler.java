package cz.muni.fi.pv168.recipesdb.data.storage.database;

import java.io.Closeable;

/**
 * @author Vít Šebela
 */
public interface TransactionHandler extends Closeable {

    /**
     * @return active {@link ConnectionHandler} instance
     */
    ConnectionHandler connection();

    /**
     * Commits active transaction
     */
    void commit();

    /**
     * Closes active connection
     */
    void close();
}
