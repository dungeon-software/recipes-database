package cz.muni.fi.pv168.recipesdb.data.storage.valueobject;

import java.util.Map;

/**
 * @author Vít Šebela
 */
public record RecipeValueObject(
        String name,
        String category,
        Map<String, Double> ingredients,
        Integer estimatedDuration,
        Integer portions,
        String description,
        String instruction
) {
    public RecipeValueObject(String name,
                             String category,
                             Map<String, Double> ingredients,
                             Integer estimatedDuration,
                             Integer portions,
                             String description,
                             String instruction) {
        this.name = name;
        this.category = category;
        this.estimatedDuration = estimatedDuration;
        this.ingredients = ingredients;
        this.portions = portions;
        this.description = description;
        this.instruction = instruction;
    }
}
