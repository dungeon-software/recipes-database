package cz.muni.fi.pv168.recipesdb.ui.dialog;

import cz.muni.fi.pv168.recipesdb.data.DataAccess;
import cz.muni.fi.pv168.recipesdb.model.Category;
import cz.muni.fi.pv168.recipesdb.model.Recipe;
import cz.muni.fi.pv168.recipesdb.model.WeightedIngredient;
import cz.muni.fi.pv168.recipesdb.ui.controller.TableCreator;
import cz.muni.fi.pv168.recipesdb.ui.model.ComboBoxModelAdapter;
import cz.muni.fi.pv168.recipesdb.ui.model.GenericTableModel;
import cz.muni.fi.pv168.recipesdb.util.Notificator;
import java.util.ArrayList;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class RecipeDialog extends EntityDialog<Recipe> {
    private final JTextField portions = new JTextField();
    private final JTextField name = new JTextField();
    private final JTextArea description = new JTextArea(2,10);
    private final JTextArea instructions = new JTextArea(5,10);
    private final JTextField estimatedTime = new JTextField();
    private final ComboBoxModel<Category> categoryListModel;
    private JTable weightedIngredients;
    private final Recipe recipe;

    private RecipeDialog(Recipe recipe) {
        this.recipe = recipe;
        this.categoryListModel = new ComboBoxModelAdapter<>(DataAccess.getCategoryListModel());
        setInitialValues();
        addFields();
    }

    public static <T> RecipeDialog createRecipeDialog(T recipe) {
        return new RecipeDialog((Recipe) recipe);
    }

    public static RecipeDialog createEmptyRecipeDialog() {
        if (DataAccess.getCategoryList().isEmpty()) {
            Notificator.showMissingCategoriesNotification();
        }
        return new RecipeDialog(
                new Recipe("",
                        DataAccess.getCategoryList().get(0),
                        0,
                        0,
                        "",
                        "",
                        new ArrayList<>()));
    }

    private void setInitialValues() {
        name.setText(recipe.getName());
        portions.setText(recipe.getPortions().toString());
        description.setText(recipe.getDescription());
        instructions.setText(recipe.getInstructions());
        estimatedTime.setText(recipe.getEstimatedDuration().toString());
        categoryListModel.setSelectedItem(recipe.getCategory());
        weightedIngredients = TableCreator.createRecipeIngredientDialogTable(recipe.getWeightedIngredients());
    }

    private void addFields() {
        if (name.getText().equals("")) {
            add("Name:", name);
        }
        add("Category:", new JComboBox<>(categoryListModel));
        add("Estimated duration:", estimatedTime, "min");
        add("Portions:", portions);
        add("Description:");
        createTextArea(description);
        add("Instructions:");
        createTextArea(instructions);
        add("Ingredients:");
        add(weightedIngredients);
    }

    @Override
    Recipe getEntity() {
        recipe.setName(name.getText());
        recipe.setCategory((Category) categoryListModel.getSelectedItem());
        try {
            recipe.setEstimatedDuration(Integer.parseInt(estimatedTime.getText()));
            recipe.setPortions(Integer.parseInt(portions.getText()));
        } catch (NumberFormatException nfe) {
            Notificator.showUnparseableNumberNotification("Either portions number or estimated duration", "integer");
        }
        recipe.setDescription(description.getText());
        recipe.setInstructions(instructions.getText());
        recipe.setWeightedIngredients(((GenericTableModel<WeightedIngredient>) weightedIngredients.getModel()).getRecords());
        // TODO: The recipe probably needs to be explicitly updated since weightedIngredients don't use repository
        return recipe;
    }
}
