package cz.muni.fi.pv168.recipesdb.ui.filter.rowfilter;

import cz.muni.fi.pv168.recipesdb.model.Ingredient;
import cz.muni.fi.pv168.recipesdb.model.Recipe;
import cz.muni.fi.pv168.recipesdb.model.WeightedIngredient;
import cz.muni.fi.pv168.recipesdb.ui.model.GenericTableModel;
import javax.swing.JTable;
import javax.swing.RowFilter;

public class IngredientRowFilter extends RowFilter<Object, Object> {
    private final WeightedIngredient weightedIngredient;
    private final JTable table;

    public IngredientRowFilter(JTable table, Ingredient ingredient) {
        this.table = table;
        this.weightedIngredient = new WeightedIngredient(ingredient);
    }

    @Override
    public boolean include(Entry entry) {
        GenericTableModel<Recipe> tableModel = (GenericTableModel) table.getModel();
        Recipe recipe = tableModel.getEntity((Integer) entry.getIdentifier());

        return recipe.getWeightedIngredients().contains(weightedIngredient);
    }
}
