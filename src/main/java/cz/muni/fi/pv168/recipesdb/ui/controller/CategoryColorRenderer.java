package cz.muni.fi.pv168.recipesdb.ui.controller;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import org.beryx.awt.color.ColorFactory;

/**
 * @author Vít Šebela
 */
public class CategoryColorRenderer extends DefaultTableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table,
                                                   Object value,
                                                   boolean isSelected,
                                                   boolean hasFocus,
                                                   int row,
                                                   int column) {
        Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        if (column == 1) {
            try {
                cell.setBackground(ColorFactory.valueOf(getText()));
            } catch (IllegalArgumentException e) {
                cell.setBackground(Color.white);
            }
        }
        return cell;
    }
}
