package cz.muni.fi.pv168.recipesdb.data.storage.valueobject;

/**
 * @author Vít Šebela
 */
public record CategoryValueObject(String name, String color){
    public CategoryValueObject(String name, String color) {
        this.name = name;
        this.color = color;
    }
}
