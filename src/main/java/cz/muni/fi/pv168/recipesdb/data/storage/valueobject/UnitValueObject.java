package cz.muni.fi.pv168.recipesdb.data.storage.valueobject;

/**
 * @author Vít Šebela
 */
public record UnitValueObject(String name, String acronym, double amount, String baseUnit){
    public UnitValueObject(String name, String acronym, double amount, String baseUnit) {
        this.name = name;
        this.acronym = acronym;
        this.amount = amount;
        this.baseUnit = baseUnit;
    }
}
