package cz.muni.fi.pv168.recipesdb.ui.dialog;

import cz.muni.fi.pv168.recipesdb.data.DataAccess;
import cz.muni.fi.pv168.recipesdb.model.Ingredient;
import cz.muni.fi.pv168.recipesdb.model.Unit;
import cz.muni.fi.pv168.recipesdb.ui.model.ComboBoxModelAdapter;
import cz.muni.fi.pv168.recipesdb.util.Notificator;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTextField;

public class IngredientDialog extends EntityDialog<Ingredient> {
    private final JTextField name = new JTextField();
    private final ComboBoxModel<Unit> units = new ComboBoxModelAdapter<>(DataAccess.getUnitListModel());
    private final JTextField nutritionValue = new JTextField();
    private final Ingredient ingredient;

    private IngredientDialog(Ingredient ingredient) {
        this.ingredient = ingredient;
        setInitialValues();
        addFields();
    }

    public static <T> IngredientDialog createIngredientDialog(T ingredient) {
        return new IngredientDialog((Ingredient) ingredient);
    }

    public static IngredientDialog createEmptyIngredientDialog() {
        return new IngredientDialog(
                new Ingredient("",
                        DataAccess.getBaseUnit("KG"),
                        0));
    }

    private void setInitialValues() {
        name.setText(ingredient.getName());
        units.setSelectedItem(ingredient.getUnit());
        nutritionValue.setText(ingredient.getNutritionalValue().toString());
    }

    private void addFields() {
        if (name.getText().equals("")) {
            add("Name:", name);
        }
        add("Units:", new JComboBox<>(units));
        add("Nutritional Value:", nutritionValue, "kcal/[unit]");
    }

    @Override
    Ingredient getEntity() {
        ingredient.setName(name.getText());
        ingredient.setUnit((Unit) units.getSelectedItem());
        try {
            ingredient.setNutritionalValue(Integer.parseInt(nutritionValue.getText()));

        } catch (NumberFormatException nfe) {
            Notificator.showUnparseableNumberNotification("Nutritional value","integer");
            return null;
        }
        return ingredient;
    }
}
