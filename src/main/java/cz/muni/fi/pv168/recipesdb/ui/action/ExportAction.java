package cz.muni.fi.pv168.recipesdb.ui.action;

import cz.muni.fi.pv168.recipesdb.data.manipulation.Exporter;
import cz.muni.fi.pv168.recipesdb.ui.model.GenericTableModel;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Objects;
import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JTable;
import javax.swing.KeyStroke;

/**
 * @author Vít Šebela
 */
public class ExportAction extends AbstractAction {
    private final JTable table;
    private final Exporter exporter;

    public ExportAction(JTable table, Exporter exporter) {
        this.table = Objects.requireNonNull(table);
        this.exporter = Objects.requireNonNull(exporter);
        putValue(SHORT_DESCRIPTION, "Exports recipes to a file");
        putValue(MNEMONIC_KEY, KeyEvent.VK_X);
        putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke("ctrl X"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        var fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));

        int dialogResult = fileChooser.showSaveDialog(table);
        if (dialogResult == JFileChooser.APPROVE_OPTION) {
            File exportFile = fileChooser.getSelectedFile();
            var recipeTableModel = (GenericTableModel) table.getModel();
            exporter.exportRecipes(recipeTableModel.getRecords(), exportFile.getAbsolutePath());
        }
    }
}
