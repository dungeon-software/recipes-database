package cz.muni.fi.pv168.recipesdb.ui.component;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import net.miginfocom.swing.MigLayout;

public class ScrollPanePanel extends JPanel {
    public ScrollPanePanel(JTable table) {
        super();
        setLayout(new MigLayout( "", "fill, grow", "fill, grow"));
        add(new JScrollPane(table));
    }
}
