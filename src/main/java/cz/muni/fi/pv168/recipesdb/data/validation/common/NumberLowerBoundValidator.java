package cz.muni.fi.pv168.recipesdb.data.validation.common;

import cz.muni.fi.pv168.recipesdb.data.validation.ValidationResult;
import cz.muni.fi.pv168.recipesdb.util.Notificator;

public final class NumberLowerBoundValidator extends PropertyValidator<Integer> {
    private final int lowerBound;

    public NumberLowerBoundValidator(int lowerBound, String name) {
        super(name);
        this.lowerBound = lowerBound;
    }

    @Override
    public ValidationResult validate(Integer number) {
        var result = new ValidationResult();

        if ( lowerBound > number ) {
            result.add("'%s' can't be lower than %d"
                    .formatted(getName(number), lowerBound)
            );
            Notificator.showNumberLowerBoundValidatorErrorNotification(getName(number), lowerBound);
        }

        return result;
    }
}

