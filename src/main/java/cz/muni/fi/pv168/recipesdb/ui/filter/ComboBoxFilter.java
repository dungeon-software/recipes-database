package cz.muni.fi.pv168.recipesdb.ui.filter;

import cz.muni.fi.pv168.recipesdb.data.DataAccess;
import cz.muni.fi.pv168.recipesdb.model.Category;
import cz.muni.fi.pv168.recipesdb.model.Ingredient;
import cz.muni.fi.pv168.recipesdb.ui.filter.rowfilter.CategoryRowFilter;
import cz.muni.fi.pv168.recipesdb.ui.filter.rowfilter.IngredientRowFilter;
import cz.muni.fi.pv168.recipesdb.ui.filter.rowfilter.TrueRowFilter;

import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.RowFilter;
import java.util.Objects;

public class ComboBoxFilter implements Filter {
    private final JTable table = DataAccess.getRecipeTable();
    private final ComboBoxFilterType filterType;
    private final JComboBox<Object> comboBox;

    public ComboBoxFilter(ComboBoxFilterType comboBoxFilterType, JComboBox<Object> comboBox) {
        this.filterType = comboBoxFilterType;
        this.comboBox = comboBox;
    }

    public JTable getTable() {
        return table;
    }

    @Override
    public RowFilter<Object, Object> getRowFilter() {
        if (Objects.requireNonNull(comboBox.getSelectedItem()).toString().equals("All")) {
            return new TrueRowFilter();
        }

        switch (filterType) {
            case RECIPE_CATEGORY -> {
                return new CategoryRowFilter(table, (Category) comboBox.getSelectedItem());
            }
            case RECIPE_INGREDIENT -> {
                return new IngredientRowFilter(table, (Ingredient) comboBox.getSelectedItem());
            }
            default -> throw new IllegalStateException("Filter type not supported");
        }
    }

    @Override
    public void resetFilter() {
        comboBox.setSelectedItem("All");
    }

    @Override
    public boolean contentIsValid() {
        return true;
    }
}
