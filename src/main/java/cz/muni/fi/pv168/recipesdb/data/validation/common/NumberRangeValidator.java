package cz.muni.fi.pv168.recipesdb.data.validation.common;

import cz.muni.fi.pv168.recipesdb.data.validation.ValidationResult;
import cz.muni.fi.pv168.recipesdb.util.Notificator;

public final class NumberRangeValidator extends PropertyValidator<Integer> {
    private final int lowerBound;
    private final int upperBound;

    public NumberRangeValidator(int lowerBound, int upperBound, String name) {
        super(name);
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
    }

    @Override
    public ValidationResult validate(Integer number) {
        var result = new ValidationResult();

        if ( lowerBound > number ||  number >= upperBound ) {
            result.add("'%s' is not between %d (inclusive) and %d (exclusive)"
                    .formatted(getName(number), lowerBound, upperBound)
            );
            Notificator.showNumberRangeValidatorErrorNotification(getName(number), lowerBound, upperBound);
        }

        return result;
    }
}
