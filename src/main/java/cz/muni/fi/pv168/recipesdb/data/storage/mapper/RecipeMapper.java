package cz.muni.fi.pv168.recipesdb.data.storage.mapper;

import cz.muni.fi.pv168.recipesdb.data.storage.DataStorageException;
import cz.muni.fi.pv168.recipesdb.data.storage.valueobject.RecipeValueObject;
import cz.muni.fi.pv168.recipesdb.data.storage.repository.Repository;
import cz.muni.fi.pv168.recipesdb.data.validation.Validator;
import cz.muni.fi.pv168.recipesdb.model.Category;
import cz.muni.fi.pv168.recipesdb.model.Ingredient;
import cz.muni.fi.pv168.recipesdb.model.Recipe;
import cz.muni.fi.pv168.recipesdb.model.WeightedIngredient;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Vít Šebela
 */
public class RecipeMapper implements EntityMapper<RecipeValueObject, Recipe> {
    private final Repository<Ingredient> ingredientRepository;
    private final Repository<Category>  categoryRepository;
    private final Validator<Recipe> recipeValidator;

    public RecipeMapper(Repository<Ingredient> ingredientRepository, Repository<Category> categoryRepository,
                        Validator<Recipe> recipeValidator) {
        this.ingredientRepository = ingredientRepository;
        this.categoryRepository = categoryRepository;
        this.recipeValidator = recipeValidator;
    }

    @Override
    public RecipeValueObject mapToDatabaseEntity(Recipe model) {
        recipeValidator.validate(model).intoException();

        Map<String, Double> ingredients = new HashMap<>();
        model.getWeightedIngredients().forEach(ingredient -> {
            ingredients.put(
                    ingredient.getIngredient().getName(),
                    ingredient.getAmount());
        });
        return new RecipeValueObject(
                model.getName(),
                model.getCategory().getName(),
                ingredients,
                model.getEstimatedDuration(),
                model.getPortions(),
                model.getDescription(),
                model.getInstructions()
        );
    }

    @Override
    public Recipe mapToModel(RecipeValueObject entity) {

        var recipeIngredients = ingredientRepository.findAll()
                .stream()
                .filter(ingredient ->
                        entity.ingredients().keySet()
                                .stream()
                                .toList()
                                .contains(ingredient.getName()))
                .toList()
                .stream()
                .map(ingredient -> new WeightedIngredient(
                        ingredient,
                        entity.ingredients()
                                .get(ingredient.getName())))
                .toList();

        return new Recipe(
                entity.name(),
                categoryRepository.findAll().stream()
                        .filter(category -> category.getName().equals(entity.category()))
                        .findFirst()
                        .orElseThrow(() -> new DataStorageException("Non existent category of recipe")),
                entity.estimatedDuration(),
                entity.portions(),
                entity.description(),
                entity.instruction(),
                recipeIngredients
        );
    }
}
