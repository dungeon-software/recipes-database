package cz.muni.fi.pv168.recipesdb.ui.component;

import cz.muni.fi.pv168.recipesdb.data.DataAccess;
import cz.muni.fi.pv168.recipesdb.data.manipulation.RecipePdfExporter;
import cz.muni.fi.pv168.recipesdb.ui.action.AddAction;
import cz.muni.fi.pv168.recipesdb.ui.action.ExportAction;
import cz.muni.fi.pv168.recipesdb.ui.action.ImportAction;
import cz.muni.fi.pv168.recipesdb.ui.action.QuitAction;
import cz.muni.fi.pv168.recipesdb.ui.dialog.CategoryDialog;
import cz.muni.fi.pv168.recipesdb.ui.dialog.IngredientDialog;
import cz.muni.fi.pv168.recipesdb.ui.dialog.RecipeDialog;
import cz.muni.fi.pv168.recipesdb.ui.dialog.UnitDialog;
import cz.muni.fi.pv168.recipesdb.util.Statistics;
import cz.muni.fi.pv168.recipesdb.wiring.DependencyProvider;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JToolBar;
import javax.swing.WindowConstants;


public class MainWindow extends JFrame {
    private final DependencyProvider dependencyProvider = DataAccess.getDependencyProvider();
    private final JTable recipeTable = DataAccess.getRecipeTable();
    private final JTable categoryTable = DataAccess.getCategoryTable();
    private final JTable unitTable = DataAccess.getUnitTable();
    private final JTable ingredientTable = DataAccess.getIngredientTable();

    public MainWindow() {
        super("Recipes DB");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        addTabs();
        addMenuBar();
        addToolbar();

        pack();
    }

    private void addMenuBar() {
        JMenuBar menuBar = new JMenuBar();

        menuBar.add(createFileMenu());
        menuBar.add(createHelpMenu());

        setJMenuBar(menuBar);
    }

    private JMenu createFileMenu() {
        JMenu fileMenu = new JMenu("File");
        fileMenu.setMnemonic('e');

        fileMenu.add(createStatistics());
        fileMenu.addSeparator();
        fileMenu.add(createImport());
        fileMenu.addSeparator();
        fileMenu.add(createPDFExport());
        fileMenu.add(createJSONExport());
        fileMenu.addSeparator();
        fileMenu.add(new QuitAction());

        return fileMenu;
    }

    private JMenuItem createJSONExport() {
        JMenuItem exportJson = new JMenuItem("Export to JSON");
        exportJson.addActionListener(new ExportAction(recipeTable, dependencyProvider.getExporter()));
        exportJson.setToolTipText("Exports data to selected file in JSON format");
        return exportJson;
    }

    private JMenuItem createPDFExport() {
        JMenuItem exportPdf = new JMenuItem("Export to PDF");
        exportPdf.addActionListener(new ExportAction(recipeTable, new RecipePdfExporter()));
        exportPdf.setToolTipText("Exports data to selected file in PDF format");
        return exportPdf;
    }

    private JMenuItem createImport() {
        JMenuItem importItem = new JMenuItem("Import");
        importItem.addActionListener(new ImportAction(recipeTable, dependencyProvider.getImporter()));
        importItem.setToolTipText("Imports data");
        return importItem;
    }

    private JMenuItem createStatistics() {
        JMenuItem statistics = new JMenuItem("Statistics");
        statistics.addActionListener(this::showStatistics);
        statistics.setToolTipText("Shows statistics");
        return statistics;
    }

    private JMenu createHelpMenu() {
        JMenu helpMenu = new JMenu("Help");

        JMenuItem infoItem = new JMenuItem("Info");
        infoItem.addActionListener(this::info);
        infoItem.setToolTipText("Shows information about the application");
        helpMenu.add(infoItem);

        return helpMenu;
    }

    private void info(ActionEvent e) {
        JOptionPane.showMessageDialog(this,
                """
                        Project for course PV168 - Java Seminar
                        This is a desktop application made for tracking recipes.
                        It allows you to browse recipes, track ingredients, instructions and all other necessary information about them.
                        Additionally, the app lets you create custom categories and units.
                        
                        Authors:
                            Vít Šebela
                            Stanislav Zeman
                            Martin Samec
                            Lukáš Málik""",
                "Information", JOptionPane.INFORMATION_MESSAGE);
    }

    private void showStatistics(ActionEvent e) {
        JOptionPane.showMessageDialog(this, Statistics.generateStatistics(),
                "Statistics", JOptionPane.PLAIN_MESSAGE);
    }

    /**
     * Toolbar is the second bar from top with Add Actions.
     */
    private void addToolbar() {
        JToolBar toolbar = new JToolBar();

        AbstractAction addRecipeAction = new AddAction<>(
                recipeTable,
                "Add Recipe",
                "Add a new recipe",
                RecipeDialog::createEmptyRecipeDialog);

        AbstractAction addCategoryAction = new AddAction<>(
                categoryTable,
                "Add Category",
                "Add a new category",
                CategoryDialog::createEmptyCategoryDialog);

        AbstractAction addIngredientAction = new AddAction<>(
                ingredientTable,
                "Add Ingredient",
                "Add a new ingredient",
                IngredientDialog::createEmptyIngredientDialog);

        AbstractAction addUnitAction = new AddAction<>(
                unitTable,
                "Add Unit",
                "Add a new unit",
                UnitDialog::createEmptyUnitDialog);

        toolbar.add(addRecipeAction);
        toolbar.addSeparator();
        toolbar.add(addCategoryAction);
        toolbar.addSeparator();
        toolbar.add(addIngredientAction);
        toolbar.addSeparator();
        toolbar.add(addUnitAction);

        add(toolbar, BorderLayout.BEFORE_FIRST_LINE);
    }

    private void addTabs() {
        JTabbedPane tabbedPane = new JTabbedPane();

        tabbedPane.addTab("Recipes", null, new RecipePanel(recipeTable, dependencyProvider),
                "Recipes");
        tabbedPane.addTab("Categories", null, new ScrollPanePanel(categoryTable), "Categories");
        tabbedPane.addTab("Ingredients", null, new ScrollPanePanel(ingredientTable), "Ingredients");
        tabbedPane.addTab("Units", null, new ScrollPanePanel(unitTable), "Units");

        getContentPane().add(tabbedPane);
    }
}