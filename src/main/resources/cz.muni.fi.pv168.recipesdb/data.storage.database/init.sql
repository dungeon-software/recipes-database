CREATE TABLE IF NOT EXISTS Category
(
    name  VARCHAR(50) PRIMARY KEY,
    color VARCHAR(50) CHECK (color in ('WHITE',
                                       'BLACK',
                                       'GRAY',
                                       'BLUE',
                                       'LIGHTBLUE',
                                       'DARKBLUE',
                                       'ORANGE',
                                       'MAGENTA',
                                       'PINK',
                                       'RED',
                                       'LIGHTRED',
                                       'DARKRED',
                                       'YELLOW',
                                       'LIGHTYELLOW',
                                       'DARKYELLOW',
                                       'GREEN',
                                       'LIGHTGREEN',
                                       'DARKGREEN',
                                       'BROWN',
                                       'PURPLE',
                                       'CYAN',
                                       'TURQUOISE',
                                       'VIOLET',
                                       'WHEAT',
                                       'WHITESMOKE',
                                       'LAVENDER',
                                       'KHAKI',
                                       'IVORY',
                                       'GOLD')) NOT NULL
);

CREATE TABLE IF NOT EXISTS Unit
(
    name     VARCHAR(50) PRIMARY KEY,
    acronym  VARCHAR(50)                                            NOT NULL,
    amount   FLOAT(2)                                               NOT NULL,
    baseUnit VARCHAR(50) CHECK (baseUnit in ('kg', 'g', 'ml', 'l')) NOT NULL
);

CREATE TABLE IF NOT EXISTS Ingredient
(
    name             VARCHAR(50) PRIMARY KEY,
    unit             VARCHAR(50) REFERENCES Unit (name),
    nutritionalValue INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS Recipe
(
    name              VARCHAR(50) PRIMARY KEY,
    category          VARCHAR(50) REFERENCES Category (name),
    estimatedDuration INTEGER           NOT NULL,
    portions          INTEGER           NOT NULL,
    description       CHARACTER VARYING NOT NULL,
    instructions      CHARACTER VARYING NOT NULL
);

CREATE TABLE IF NOT EXISTS RecipeIngredients
(
    recipe     VARCHAR(50) REFERENCES Recipe (name),
    ingredient VARCHAR(50) REFERENCES Ingredient (name),
    amount     FLOAT(2) NOT NULL
);

MERGE INTO Unit (name, acronym, amount, baseUnit)
VALUES ('kilogram','kg',1,'kg'),
       ('gram','g',1,'g'),
       ('liter','l',1,'l'),
       ('mililiter','ml',1,'ml')
;

